natbraille (2.0rc3-15) unstable; urgency=medium

  * rules: Bump java compatibility to 1.8 (Closes: Bug#1053064)
  * source/lintian-overrides: Ignore lintian false positive.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 26 Dec 2023 01:21:42 +0100

natbraille (2.0rc3-14) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Make Multi-Arch: foreign.
  * patches/tags: Fix tags in documentation (Closes: #1026581)

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
    + Drop check for DEB_BUILD_OPTIONS containing "nocheck", since debhelper now
      does this.
  * Set upstream metadata fields: Archive, Bug-Database, Name.
  * Update standards version to 4.6.1, no changes needed.
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends-Indep: Drop versioned constraint on writer2latex.
    + natbraille: Drop versioned constraint on libsaxonb-java and writer2latex
      in Depends.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 20 Dec 2022 19:42:05 +0100

natbraille (2.0rc3-13) unstable; urgency=medium

  * patches/x: Avoid +x permissions on audio files.
  * control: Bump Standards-Version to 4.6.0 (no change)
  * rules: Bump java compatibility to 1.7 (Closes: #1011607)

 -- Samuel Thibault <sthibault@debian.org>  Wed, 25 May 2022 17:21:08 +0200

natbraille (2.0rc3-12) unstable; urgency=medium

  * control: Do not force installing the default java runtime.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 16 Nov 2021 20:10:09 +0100

natbraille (2.0rc3-11) unstable; urgency=medium

  * rules: Fix reproducibility by disabling javadoc translations.
  * control: Set Rules-Requires-Root to no.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 26 Oct 2021 00:37:16 +0200

natbraille (2.0rc3-10) unstable; urgency=medium

  [ Rene Engelhard ]
  * explicitly (Build-)Depend on libridl-java, libjurt-java, libjuh-java,
    libunoil-java (closes: #965145)

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.5.0, no changes needed.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 02 Aug 2020 01:48:21 +0200

natbraille (2.0rc3-9) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Set Vcs-* to salsa.debian.org.
  * control: Bump Standards-Version to 4.4.0 (no changes).
  * tests/control: Mark test as superficial.
  * control: Update alioth list domain.

  [ Olivier Tilloy ]
  * Update runtime dependency on the transitional dummy ttf-dejavu-core
    (which was dropped from unstable) to fonts-dejavu-core (Closes: #962201)

 -- Samuel Thibault <sthibault@debian.org>  Wed, 10 Jun 2020 09:35:44 +0200

natbraille (2.0rc3-8) unstable; urgency=medium

  * Bump Standards-Version to 4.2.0 (no changes).
  * tests/control: Add autopkgtest.
  * control: Mark natbraille-doc Multi-Arch: foreign.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 23 Oct 2018 18:32:58 +0200

natbraille (2.0rc3-7) unstable; urgency=medium

  * rules: Bump java compatibility to 1.6
  * rules: Fix javadoc call.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 17 Mar 2018 11:55:50 +0100

natbraille (2.0rc3-6) unstable; urgency=medium

  * rules: While checking output, use diff -u output. Support nocheck build
    option.
  * patches/nonetwork: Use sed to replace network DTD link with on-disk link
    (Closes: #859957)
  * debian/testresults/documents/testMathOpenOffice.odt.txt: Update accordingly
    (natbrailles' DTD are a bit different)

 -- Samuel Thibault <sthibault@debian.org>  Mon, 20 Nov 2017 23:19:28 +0100

natbraille (2.0rc3-5) unstable; urgency=medium

  * Use canonical anonscm vcs URL.
  * control: Update maintainer mailing list.
  * copyright: Remove copy of MPL 1.1, now in common-licences.
  * control: Migrate priority to optional.
  * control: Bump Standards-Version to 4.1.1 (no change)

 -- Samuel Thibault <sthibault@debian.org>  Tue, 07 Nov 2017 02:17:27 +0100

natbraille (2.0rc3-4) unstable; urgency=medium

  * menu: convert to natbraille.desktop file.
  * Bump Standards-Version to 3.9.8.
  * HOME.diff: Take $HOME into account before ~.
  * test.odt, bin/natbraille-bin, rules, control: Add simple testcase.
  * rules: Use CURDIR instead of PWD.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 04 Sep 2016 18:49:45 +0200

natbraille (2.0rc3-3) unstable; urgency=low

  * Bump Standards-Version to 3.9.7.
    - control: Remove java2-runtime Depends alternative.
  * rules:
    - Keep compatibility with Java5.
    - Pass -notimestamp to javadoc to make build more reproducible.
  * watch: Fix spurious dot.
  * compat: Bump to 9.
  * rules: Clear.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 23 Feb 2016 01:07:09 +0100

natbraille (2.0rc3-2) unstable; urgency=low

  * control:
    - Bump Standards-Version to 3.9.3 (no changes).
    - Add libjopt-simple-java runtime dependency.
    - Depends on libreoffice-java-common instead of openoffice-java-common.
    (Closes: Bug#707549)
  * natbraille.install: Include icons and sounds.
  * watch: Add.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 02 Mar 2012 00:28:21 +0100

natbraille (2.0rc3-1) unstable; urgency=low

  * Initial release (Closes: Bug#530432)

 -- Samuel Thibault <sthibault@debian.org>  Tue, 24 Jan 2012 01:59:23 +0100
