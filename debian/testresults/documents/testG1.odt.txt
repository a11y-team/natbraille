  ¨¨jeu de test

  ¨caractères et expressions
littéraires prises en compte
par ¨¨nat v`1.2

  ¨braille ¨intégral, ¨nou-
velle norme `2006

  ¨abréviations utilisées:

  cn: caractère noir

  cb: caractère braille


         ¨caractères

        ¨dans l'ordre
         alphabétique

    `1 cn `" `1 cb `! majus-
    cules
  a ¨a b ¨b c ¨c d ¨d e ¨e f
¨f g ¨g h ¨h i ¨i j ¨j k ¨k l
¨l m ¨m n ¨n o ¨o p ¨p q ¨q r
¨r s ¨s t ¨t u ¨u v ¨v w ¨w x
¨x y ¨y z ¨z                `2

  à ¨à 1 ¨1 é ¨é è ¨è 2 ¨2 6
¨6 3 ¨3 7 ¨7 4 ¨4 ù ¨ù 5 ¨5 8
¨8

  ç ¨ç @ ¨@ 9 ¨9

¨lettres étrangères
  à ¨à / ¨/ ó ¨ó ù ¨ù 7 ¨7

  / ¨/ ó ¨ó

  @ ¨@ 9 ¨9

  é espanol: no sé

  @

  `! `- `* `: `"

  `0'1'2'3'4'5'6'7'8'9

    `1 cn `" n cb
  -- ´c ´o ´p ´r ´t ´é ´? ´*
´0 ´ó ´óó ´-\ ¤c ¤e ¤l ¤s ¤y

  ´2 ´@ ¤2 ¤@               `3

  `3¤e et `25¤c´@2¤s´é4¤y

  `3 ¤e et `2 ¤s

         ¨ponctuation

    `1 cn `" `1 cb
  ,;:.?! " "" "" "" ()' -- --
!?

  ¨espa7ol !hola! ?come esta?

    `1 cn `" n cb
  ...... ``()'' ¤()b

  ¤(test)b (des) "espaces"
``(avec)'' "les" "ponctua-
tions" "ouvrantes" 'et' --fer-
mantes--

    ¨tirets
  expr. exp-epr. exp-gf-fff.
et...


                            `4
    ¨guillemets
  ¨un "test "imbriqué""


          ¨préfixes

         ¨majuscules

  ¨remarque: les minuscules
"a" insérées servent à éviter
les passages en majuscules

  ¨bruno ¨¨mascret

  ¨al`2¨¨so4

  ¨¨co`2¨¨--co a

  ¨¨rendez-vous a

  ¨¨l'arr2t a

  :¨les quatre filles du doc-
teur ¨march a

  ¨¨les ¨¨quatre ¨¨filles du
¨¨docteur ¨¨march a.        `5

  ¨¨les `4 :¨filles du docteur
¨march a.

  `10 :¨petits nègres, agatha
¨christie a

  ¨¨action, ré¨¨action,
¨a¨c¨t¨i¨o¨naire. a

  ¨¨le ¨¨xviii^ème ¨¨siecle a

  ¨¨le ¨¨xiii^ème ¨¨siecle
"¨¨des ¨¨lumieres" a

  ¨x¨i¨i¨ième siècle

  ¨avec ¨open¨office: ":¨pas-
sage en majuscule précédé de
¨span (guillemets par ex.).

           ¨nombres

  `1'12,4'12.4'123'333

  `1 `! `2 `" `8 `: `2 `- `1
`* `1                       `6

  `1!2"8:2-1*1

  le `1er, le `2^nd et le
`3ème

  `13km/h `2m^2

  `88„¨ha8y

  `88„¨ha8y8

  ¨ha8y`88

  ¨c`3¨c ¨w`3¨c 8`88s

  ¨le ¨l.¨i.¨r.¨i.¨s. et
l'¨u.¨c.¨b.¨l.

  ¨bonjour :¸¨le cep accueille
le ¸séminaire.





                            `7
          ¨problèmes


  ¨les "guillemets "imbri-
qués""

  open-office: le `1er, le
`2^nd et le `3ème

  un texte en^expo-
sant^et^c'est^cool texte.

  bonjour














