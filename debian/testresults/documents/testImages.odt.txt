
    ¨fichier de test pour
        la conversion
         d'images en
           ¨braille


  ¨il faut installer pour cela
¨image¨magick, dans une ver-
sion récente.

  ¨pour savoir si ¨image¨ma-
gick peut rendre en ¨braille
des images, taper dans une
console:

  convert --list format

  ¨si l'extension ¨¨brf est
présente, c'est bon!

  ¨pour améliorer l'intégra-
tion des images, indiquez un
texte alternatif à l'image;
dans open office, cette option
se trouve dans les propriétés
de l'image (se mettre sur l'i-
mage et demander le menu    `2
contextuel). ¨dans ce docu-
ment, les premières images ont
un texte alternatif, pas les
dernières.

  ¨une image qui ne va rien
rendre en braille:

  ¤(¨formes géométriques)b

  ¨une image dont les lettres
ne seront pas transcrites et
qui risque de ne pas rendre
grand chose non plus en
braille si les dimensions de
la page ne sont pas assez
grandes:

  ¤(¨triangle, hauteurs et
cercle)b

  ¨pour l'image suivante, il
vaut mieux faire `4 figures
séparées, et écrire le texte
en commentaire des figures:

  ¤(cones)b                 `3

  ¨un c4ne de révolution:
¤(¨c4ne de révolution)b

  ¨un c4ne pyramidal: ¤(¨c4ne
pyramidal)b

  ¨deux c4nes quelconques:
¤(images`6)b ¤(images`7)b

  ¨attention: la taille des
conversions dépend du fichier
d'origine: les redimensionne-
ments effectuées "à la main"
dans le traitement de texte ne
servent à rien.

  ¨voici par exemple deux rec-
tangles créés dans deux fi-
chiers séparés qui se res-
semblent à l'écran mais pas
dans leur fichier d'origine:

  ¤(¨un rectangle très long)b

  ¤(¨un rectangle très large)b
  ¨les dimensions de l'i-   `4
mage suivante ont été modi-
fiées à la main: l'image est
en réalité un disque et pas
une ellipse. ¨elle sera trai-
tée comme dans le fichier d'o-
rigine donc comme un disque.

  ¤(images`8)b



















