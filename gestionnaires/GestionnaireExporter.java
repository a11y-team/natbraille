/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package gestionnaires;

import java.awt.BorderLayout;
import java.awt.Dimension ;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.JOptionPane;

import nat.ConfigNat;

import outils.TextSender;
import outils.FileToolKit;

import java.io.File;
import java.util.Collections;
import java.util.Vector;

import ui.BrailleTableComboBoxRenderer;
import ui.BrailleTableListItem;
import ui.EditeurBraille;
import ui.FiltreFichier;

/**
 * Outils pour l'exportation des fichiers transcits
 * @author djidjo, bruno (doc)
 *
 */
public class GestionnaireExporter implements ActionListener, PropertyChangeListener
{	
	/** constante pour lexportation au format brf */
	public static final int EXPORTER_BRF = 1 ;
	
	/* attributs */
	/** JFileChooser pour la sélection du fichier*/
	private JFileChooser selectionneFichier = new JFileChooser();
	/** liste des tables brailles */
	private JComboBox comboTables ;
	/** label pour {@link #selectionneFichier}*/
	private JLabel lComboTables ;
	/** panneau pour les tables brailles */
	private JPanel panelTables ;
	/** entier représentant le type d'exportation à réaliser */
	private int action;
	/** instance de TextSender gérant le texte à exporter */
	private TextSender textSender ;
	/** l'éditeur braille qui a créé ce gestionnaire */
	private EditeurBraille parent;
	//les filtres
	/** filtre pour les fichiers textes */
	private FiltreFichier ftxt = new FiltreFichier( new String[]{"txt"}, "fichiers texte (*.txt)");
	/** filtre pour les fichiers brf */
	private FiltreFichier fbrf = new FiltreFichier( new String[]{"brf"}, "fichiers BRF (*.brf)");
	/** le gestionnaire d'erreur */
	private GestionnaireErreur gest;
	/**
	 * Constructeur par copie
	 * @param par le composant construisant cette instance
	 * @param tSender instance de TextSender gérant le texte à exporter
	 * @param a entier représentant le type d'exportation à réaliser
	 * @param g instance de GestionnaireErreur
	 */
	public GestionnaireExporter (EditeurBraille par, TextSender tSender, int a, GestionnaireErreur g)
	{
		textSender=tSender ;
		action=a;
		parent=par;
		gest=g;
		
		//la table braille courante est par défaut la table braille de conversion
		FileToolKit.copyFile(ConfigNat.getUserBrailleTableFolder()+"/Brltab.ent", ConfigNat.getUserTempFolder()+"/ConvtabIn.ent");
			
	}
	/**
	 * Redéfinition; lance l'exportation en fonction de {@link #action}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent evt) 
	{    
		selectionneFichier.setSelectedFile(new File(textSender.getOrigine()));
		 // selectionneFichier.setApproveButtonText("Exporter"); intitulé du bouton inutile pour un showDialog
		 if (action == EXPORTER_BRF)
		 {
			selectionneFichier.addChoosableFileFilter(ftxt);
			selectionneFichier.addChoosableFileFilter(fbrf);
			
	 		comboTables = new JComboBox();
			lComboTables = new JLabel("Table Braille d'export en BRF");
			//lComboTables.setLabelFor(comboTables);
	 		
			/* Liste Tables braille */
			File repertoire =new File("xsl/tablesEmbosseuse");
			File[] listTablesSys = repertoire.listFiles();
			File rep2 = new File(ConfigNat.getUserEmbossTableFolder());
			File[] listTablesUser = rep2.listFiles();
		    
			int tailleSys = listTablesSys.length;
			int tailleUser = listTablesUser.length;
			Vector<BrailleTableListItem> namesList = new Vector<BrailleTableListItem>(tailleSys + tailleUser);
			
			for(int i=0;i<tailleSys;i++)
		    {
		    	String nomTable = FileToolKit.getSysDepPath(listTablesSys[i].getAbsolutePath()) ;
		    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Embtab.ent"))
		    	{
		    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
		    		namesList.add(new BrailleTableListItem(nomTable,true));
		    	}
		    }
			for(int i=0;i<tailleUser;i++)
		    {
				String nomTable = FileToolKit.getSysDepPath(listTablesUser[i].getAbsolutePath()) ;
		    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Embtab.ent"))
		    	{
		    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
		    		namesList.add(new BrailleTableListItem(nomTable,false));
		    	}
		    }
			
			
			Collections.sort(namesList);
			comboTables  = new JComboBox(namesList);
			//comboTables.addItem("table actuelle (pas de conversion)");
			
			//sélection par défaut de la table actuelle
			String selectedTablePath = ConfigNat.getUserEmbossTableFolder();
			if(ConfigNat.getCurrentConfig().getIsSysEmbossTable()){selectedTablePath = ConfigNat.getInstallFolder()+"xsl/tablesEmbosseuse/";}
			comboTables.setSelectedItem(new BrailleTableListItem(selectedTablePath+ConfigNat.getCurrentConfig().getTableBraille(),
					ConfigNat.getCurrentConfig().getIsSysEmbossTable()));
			
			comboTables.setRenderer(new BrailleTableComboBoxRenderer());
		    comboTables.setEditable(false);
		    //comboTables.setSelectedIndex(0); // par défaut, pas de conversion de table
		    comboTables.getAccessibleContext().setAccessibleName("Choix de la table braille d'export");
		    comboTables.getAccessibleContext().setAccessibleDescription("Sélectionner à l'aide des flèches la table braille à utiliser");
		    comboTables.setToolTipText("Sélectionner la table braille d'export");
			
		    panelTables = new JPanel();
		    panelTables.setLayout(new BorderLayout());
		    panelTables.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder(""),
                    BorderFactory.createEmptyBorder(5,5,5,5)));
		    
		    // nécessité d'avoir un second panel pour le resize du dialog
		    JPanel panelTables2 = new JPanel();
		    panelTables2.setLayout(new BorderLayout());
		    //panelTables2.setSize(comboTables.getWidth(), (int)(comboTables.getFont().getSize()*3));
		    panelTables2.add(lComboTables, BorderLayout.CENTER);
		    panelTables2.add(comboTables, BorderLayout.SOUTH);
		    panelTables.add (panelTables2,BorderLayout.SOUTH);
		     
		    selectionneFichier.setDialogTitle("Nom du fichier exporté");
	 		selectionneFichier.setAccessory(panelTables);
		    //comboTables.setMaximumSize(new Dimension (comboTables.getWidth(), comboTables.getFont().getSize()));
	 		comboTables.setMaximumSize(new Dimension (comboTables.getWidth(), 12));
	 		selectionneFichier.addPropertyChangeListener(this);
	 		
	 		if (selectionneFichier.showDialog(parent,"Exporter") == JFileChooser.APPROVE_OPTION)
	 		{  
	 			String encodageExport = ConfigNat.getCurrentConfig().getBrailleEncoding() ;
	 			String fichierExport = selectionneFichier.getSelectedFile().getPath() ;
	 			
	 			if (selectionneFichier.getFileFilter()!=fbrf)
	 			{
	 				FileToolKit.saveStrToFile (textSender.getText(), fichierExport, encodageExport);
	 			}
	 			else
	 			{
	 				//JOptionPane.showMessageDialog(parent, "L'export en BRF sera bientôt implémenté");
	 				//String s = FileToolKit.LoadFileToStr("./xsl/tablesBraille/Brltab");
	 				
	 				if (comboTables.getSelectedIndex()==0) // pas de conversion
	 				{
	 					FileToolKit.saveStrToFile (textSender.getText(), fichierExport, encodageExport);
	 				}
	 				else
	 				{
	 					String fichierTemp = ConfigNat.getUserTempFolder()+"/sourceText.txt" ;
	 					String tableBraille = ConfigNat.getUserBrailleTableFolder();
	 					if(ConfigNat.getCurrentConfig().getIsSysTable()){tableBraille = ConfigNat.getInstallFolder()+"xsl/tablesBraille/";}
	 					tableBraille = tableBraille + ConfigNat.getCurrentConfig().getTableBraille();
	 					FileToolKit.saveStrToFile (textSender.getText(), fichierTemp, encodageExport);
	 					FileToolKit.convertBrailleFile(fichierTemp, fichierExport, tableBraille, 
	 							((BrailleTableListItem)comboTables.getSelectedItem()).getFilename(), gest);
	 				}
	 			}
	 			//mise à jour du nom de fichier
	 			parent.setFichier(fichierExport);
	         }
		 }
		 
	 }
	/**
	 * Active ou désactive l'interface des tables brailles suivant le type de filtre choisi
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent e)
	{
		String prop = e.getPropertyName();

		if (JFileChooser.FILE_FILTER_CHANGED_PROPERTY.equals(prop))
		{
			if (selectionneFichier.getFileFilter()==fbrf)
			{
				comboTables.setEnabled(true);
				lComboTables.setText("Table Braille d'export en BRF");
				lComboTables.setEnabled(true);
			}
			else
			{
				comboTables.setEnabled(false);
				lComboTables.setText("<html><p color='grey'>Option disponible uniquement</p><p>pour export en BRF</p></html>");
				lComboTables.setEnabled(false);
			}
		}
		
	}

}
