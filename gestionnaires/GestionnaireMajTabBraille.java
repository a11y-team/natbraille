/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package gestionnaires;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JComboBox;

import outils.FileToolKit;

import nat.ConfigNat;

import ui.BrailleTableListItem;

/**
 * Gestionnaire mettant à jour les tables brailles dans les combobox
 * @author bruno
 *
 */
public class GestionnaireMajTabBraille implements WindowListener
{
	/** liste des tables brailles */
	private JComboBox comboTables;
	/** liste des tables braille d'embossage */
	private JComboBox comboTablesImp;
	/**
	 * Constructeur
	 * @param combo1 pointe sur la liste des tables brailles
	 * @param combo2 pointe sur la liste des tables brailles d'impression
	 */
	public GestionnaireMajTabBraille (JComboBox combo1, JComboBox combo2)
	{
		comboTables = combo1;
		comboTablesImp = combo2;
	}
	
	/**
	 * Implémentation; ne fait rien
	 * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */
	public void windowActivated(WindowEvent arg0) {/*do nothing*/}

	/** 
	 * Implémentation; mets à jour les combobox des tables
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	public void windowClosed(WindowEvent arg0)
	{
		majCombo("xsl/tablesBraille",ConfigNat.getUserBrailleTableFolder(),comboTables);
		majCombo("xsl/tablesEmbosseuse",ConfigNat.getUserEmbossTableFolder(),comboTablesImp);
	}
	
	/**
	 * Mets à jour le combobox <code>combo</code> avec les tables contenues dans les répertoires 
	 * <code>sysDir</code> et <code>userDir</code>
	 * @param sysDir le répertoire système contenant les tables braille système
	 * @param userDir le répertoire contenant les tables utilisateur
	 * @param combo le comboBox à mettre à jour
	 */
	private void majCombo(String sysDir, String userDir, JComboBox combo)
	{
		BrailleTableListItem selection1 = (BrailleTableListItem) combo.getSelectedItem();
			
		combo.removeAllItems();
		/* Liste Tables braille */
		File repertoire =new File(sysDir);
		File[] listTablesSys = repertoire.listFiles();
		File rep2 = new File(userDir);
		File[] listTablesUser = rep2.listFiles();
	    
		int tailleSys = listTablesSys.length;
		int tailleUser = listTablesUser.length;
		Vector<BrailleTableListItem> namesList = new Vector<BrailleTableListItem>(tailleSys + tailleUser);
		
		for(int i=0;i<tailleSys;i++)
	    {
	    	String nomTable = FileToolKit.getSysDepPath(listTablesSys[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Brltab.ent")&& !nomTable.endsWith("Embtab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,true));
	    	}
	    }
		for(int i=0;i<tailleUser;i++)
	    {
	    	String nomTable = FileToolKit.getSysDepPath(listTablesUser[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Brltab.ent")&& !nomTable.endsWith("Embtab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,false));
	    	}
	    }
		
		Collections.sort(namesList);
	    
		for(BrailleTableListItem c : namesList)
		{
			combo.addItem(c);
		}
	    combo.setSelectedItem(selection1);
	}
	/**
	 * Implémentation; ne fait rien
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	public void windowClosing(WindowEvent arg0) {/*do nothing*/}
	/**
	 * Implémentation; ne fait rien
	 * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
	 */
	public void windowDeactivated(WindowEvent arg0) {/*do nothing*/}
	/**
	 * Implémentation; ne fait rien
	 * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
	 */
	public void windowDeiconified(WindowEvent arg0) {/*do nothing*/}
	/**
	 * Implémentation; ne fait rien
	 * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
	 */
	public void windowIconified(WindowEvent arg0){/*do nothing*/}
	/**
	 * Implémentation; ne fait rien
	 * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
	 */
	public void windowOpened(WindowEvent arg0) {/*do nothing*/}
}
