/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package gestionnaires;

import java.awt.event.*;
import javax.swing.JFileChooser;

import nat.ConfigNat;

import java.io.File;

import ui.FenetrePrinc;
import ui.FiltreFichier;

/**
 * Gestionnaire gérant les différentes actions d'ouverture de fichiers possibles
 * @author bruno
 */
public class GestionnaireOuvrir implements ActionListener
{
	//constantes
	/** Constante représentant l'ouverture du choix du fichier source*/
	public static final int OUVRIR_SOURCE = 1;
	/** Constante représentant l'ouverture du choix du filtre*/
	public static final int OUVRIR_FILTRE = 2;
	/** Constante représentant l'ouverture du choix du fichier sortie*/
	public static final int OUVRIR_SORTIE = 3;
	/** Constante représentant l'ouverture du choix du fichier à mettre en page*/
	public static final int OUVRIR_MEP = 4;
	/** Constante représentant l'ouverture du choix du fichier transcrit*/
	public static final int OUVRIR_TRANS = 5;
	/* attributs */
	/** JFileChooser sélectionnant le fichier si nécessaire*/
	private JFileChooser selectionneFichier = new JFileChooser();
	/** L'instance de la fenêtre utilisant ce Gestionnaire */
	private FenetrePrinc fenetre;
	/** Entier représentant l'action à effectuer */
	private int action;
	//les filtres
	/** filtre pour les fichiers texte*/
	private static final FiltreFichier ftxt = new FiltreFichier( new String[]{"txt"}, "fichiers texte (*.txt)");
	/** filtre pour les fichiers nat */
	private static final FiltreFichier fnat = new FiltreFichier( new String[]{"nat"}, "fichiers nat à mettre en forme (*.nat)");
	/** filtre pour les fichiers xml*/
	private static final FiltreFichier fxml = new FiltreFichier( new String[]{"xml","mml", "xhtml"},"les fichiers xml (*.xml, *.mml, *.xhtml)");
	/** filtre pour les fichiers open document*/
	private static final FiltreFichier foo = new FiltreFichier( new String[]{"sxw", "odt"}, "fichiers openoffice writer (*.sxw, *.odt)");
	/** filtre pour les fichiers brf */
	private static final FiltreFichier fbrf = new FiltreFichier( new String[]{"brf", "isobrf", "braille"}, "fichiers brailles (*.brf, *.braille, *.isobrf)");
	//constructeur
	/**
	 * Constructeur
	 * @param fen la fenêtre appelant le gestionnaire 
	 * @param a code de l'action à effectuer
	 */
	public GestionnaireOuvrir(FenetrePrinc fen, int a)
	{
		fenetre = fen;
		action = a;
		File f = new File(fenetre.getEntree().getText());
		if (f.getParent()==null)
		{
			f = new File("./documents/");
			selectionneFichier.setCurrentDirectory(f);
		}
		if (action==OUVRIR_SOURCE || action == OUVRIR_SORTIE || action == OUVRIR_TRANS)
		{
			String curDir = f.getParent();
			if (curDir == null) {curDir = ConfigNat.getUserConfigFolder();}
			selectionneFichier.setCurrentDirectory(new File(curDir));
			// ajout des filtres au JFileChooser
			selectionneFichier.addChoosableFileFilter(ftxt);
			selectionneFichier.addChoosableFileFilter(foo);
			selectionneFichier.addChoosableFileFilter(fxml);
			selectionneFichier.setAcceptAllFileFilterUsed(true);
			if(action == OUVRIR_TRANS)
			{
				selectionneFichier.addChoosableFileFilter(fbrf);
				selectionneFichier.setFileFilter(fbrf);
			}
		}
		else
		{
			selectionneFichier.setCurrentDirectory(new File("./documents/"));
			// ajout des filtres au JFileChooser
			selectionneFichier.addChoosableFileFilter(fnat);
			selectionneFichier.setAcceptAllFileFilterUsed(true);
		}
		//selectionneFichier.;
	}
	
	//méthodes
	/**
	 * Implémentation; réaliser l'ouverture prévue suivant le type {@link #action}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@SuppressWarnings("deprecation")
	public void actionPerformed(ActionEvent evt) 
	 {    
		 selectionneFichier.setApproveButtonText("Choisir ce fichier"); //intitulé du bouton
		 switch (action)
		 {
		 	case OUVRIR_SOURCE :
				selectionneFichier.setDialogTitle("Sélection du fichier noir");
		 		if (selectionneFichier.showOpenDialog(fenetre) == JFileChooser.APPROVE_OPTION)
		         {    
		 			//si un fichier est selectionné, récupérer le fichier puis son path
					fenetre.setEntree(selectionneFichier.getSelectedFile().getAbsolutePath()); 
		         }
		 		break;
		 	case OUVRIR_FILTRE :
		 		if (selectionneFichier.showOpenDialog(fenetre) == JFileChooser.APPROVE_OPTION)
		         {    
					 fenetre.setFiltre(selectionneFichier.getSelectedFile().getAbsolutePath()); //si un fichier est selectionné, récupérer le fichier puis son path
		         }
		 		break;
		 	case OUVRIR_SORTIE:
				selectionneFichier.setDialogTitle("Sélection du fichier braille");
		 		if (selectionneFichier.showOpenDialog(fenetre) == JFileChooser.APPROVE_OPTION)
		         {    
		 			//si un fichier est selectionné, récupérer le fichier puis son path
					fenetre.setSortie(selectionneFichier.getSelectedFile().getAbsolutePath()); 
		         }
		 		break;
		 	case OUVRIR_MEP:
				selectionneFichier.setDialogTitle("Sélection du fichier à mettre en page");
		 		if (selectionneFichier.showOpenDialog(fenetre) == JFileChooser.APPROVE_OPTION)
		         {    
		 			//si un fichier est selectionné, récupérer le fichier puis son path
					fenetre.afficheFichierMep(selectionneFichier.getSelectedFile().getAbsolutePath()); 
		         }
		 		
		 		break;
		 	case OUVRIR_TRANS:
				selectionneFichier.setDialogTitle("Sélection du fichier (dé)transcrit");
		 		if (selectionneFichier.showOpenDialog(fenetre) == JFileChooser.APPROVE_OPTION)
		         {    
		 			//si un fichier est selectionné, récupérer le fichier puis son path
					fenetre.afficheFichier(selectionneFichier.getSelectedFile().getAbsolutePath()); 
		         }
		 		break;
		 }
		 selectionneFichier.updateUI();
	 }

}
