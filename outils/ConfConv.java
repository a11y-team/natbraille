/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package outils;

import gestionnaires.GestionnaireErreur;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.InputStreamReader;
import java.io.File;
import java.util.Properties;

import nat.ConfigNat;
import nat.Nat;
import nat.OptNames;

/**
 * Update config file to Nat-braille 2.0 format
 * @author vivien
 *
 */
public class ConfConv {
 

    /**
     * Principe de conversion des confs :
     * version 1 : pas de fi-name NI de fi-is-sys-config
     * version 2 : le nom de la table braille ne finit pas par .ent ET 
     * 		le filtre fi-filter-filename est ./xsl/xsl.xsl
     * version 3 et plus : à partir de là, la propriété conf_version sera stockée,
     * 		pour le moment elle est rajoutée automatiquement
     * 1. Liste toutes les confs d'abord dans ./configurations/ , les convertit et
     *		les déplace si nécessaire. Essaie d'écrire dans ./configurations mais si
     *		ça marche pas, ne fait que copier la conf en la convertissant dans le rép
     *		de l'utilisateur
     *
     * 2. Fait de même avec les confs perso (qui ne peuvent pas être version 1)
     * @param g L'instance de gestionnaire d'erreur
     */
    public static void convert(GestionnaireErreur g) {
		
		g.afficheMessage("\n** Vérification des fichiers de configuration...", Nat.LOG_NORMAL);
		File oldConfsDir  = new File("./configurations/");		
		File[] sysFiles =  oldConfsDir.listFiles();
		File newConfsDir = new File (ConfigNat.getUserConfigFilterFolder());
		File[] userFiles = newConfsDir.listFiles();
		
		File[] allFiles = new File[sysFiles.length+userFiles.length];
		/* arraycopy(Object src, int srcPos, Object dest, int destPos, int length)  */
		System.arraycopy(sysFiles, 0, allFiles, 0, sysFiles.length);
		System.arraycopy(userFiles, 0, allFiles, sysFiles.length, userFiles.length);
		
		for (int i=0;i<allFiles.length;i++){
			//g.afficheMessage(allFiles[i].getName(), Nat.LOG_DEBUG);			
			Properties conf = new Properties();			
			try {
				if (allFiles[i].isDirectory()) {continue;}
				FileInputStream fis = new FileInputStream(allFiles[i]);
				//InputStreamReader isr = new InputStreamReader(fis,"UTF-8");
				conf.load(fis);
				//isr.close();
				fis.close();
				if (!allFiles[i].getName().endsWith(".cfg") && !conf.containsKey(OptNames.fi_name)) {continue;}
				String version = conf.getProperty(OptNames.conf_version, "0") ;
				if (version == "0") {
					if (!(conf.containsKey(OptNames.fi_name) || conf.containsKey(OptNames.fi_is_sys_config)))
					{ version = "1" ;}
					else if (!conf.getProperty(OptNames.fi_braille_table,"").endsWith(".ent") ||
							!conf.containsKey(OptNames.fi_is_sys_braille_table) ||
							conf.getProperty(OptNames.fi_filter_filename, "").contains("./xsl/xsl.xsl"))
					{ version = "2" ;}
					else if (conf.containsKey(OptNames.fi_is_sys_braille_table) &&
							conf.containsKey(OptNames.fi_is_sys_emboss_table))
					{ version = "3" ;}
					g.afficheMessage("Configuration \""+allFiles[i].getName()+"\" considérée de version "+version, Nat.LOG_DEBUG);
				}
				else 
				{
					g.afficheMessage("Configuration \""+allFiles[i].getName()+"\" de version "+version, Nat.LOG_DEBUG);
				}
				int versionNum = 0;
				try {versionNum = Integer.parseInt(version);}
				catch (NumberFormatException nfe){
					g.afficheMessage("Problème de conversion du n° de version en entier ; version : "+version, Nat.LOG_SILENCIEUX);
				}
				
				if (versionNum == 0)
				{ g.afficheMessage("version de cette conf non détectée", Nat.LOG_SILENCIEUX);}
				else 
				{ 
					if (versionNum == 1)
					{
						conv1to2(conf,allFiles[i],g);
					}
					if (versionNum <= 2)
					{
						File f ;
						if (versionNum == 1)
							{f = new File (ConfigNat.getUserConfigFilterFolder()+allFiles[i].getName());}
						else
							{f = allFiles[i];}
						conv2to3(conf,f,g);
					}
					if (versionNum == Integer.parseInt(Nat.CONFS_VERSION) &&
						!conf.containsKey(OptNames.conf_version))
					{
						conf.setProperty(OptNames.conf_version, Nat.CONFS_VERSION);
						conf.store(new FileOutputStream(allFiles[i]), null);
						g.afficheMessage("clé de version enregistrée", Nat.LOG_DEBUG);
					}
				}
			} catch (Exception ioe) {
				ioe.printStackTrace();
				g.afficheMessage("Problème avec la configuration "+allFiles[i], Nat.LOG_SILENCIEUX);
				g.afficheMessage(ioe.getLocalizedMessage(), Nat.LOG_SILENCIEUX);
			}
		}
		
		/* bilan
		String chaine = "";
		if (chaineok.length() != 0){
				chaine += "les conf suivantes ont ete mises a jour \n"+ chaineok +"\n";
		}
		if (chaineko.length() != 0){
				chaine += "les conf suivantes ont connu un prob lors de la maj\n"+ chaineko +"\n";
		}
		if (chaine.length() == 0){
				chaine += "aucune operation effectuee\n";
		
		}*/
		//javax.swing.JOptionPane.showMessageDialog(null,"mise a jour des configurations \n" + chaine); 		
		g.afficheMessage("\n** Vérification terminée ", Nat.LOG_NORMAL);

	}	

    /**
     * Change une configuration version 1 en configuration version 2
     * @param conf l'instance de Properties représenant la configuration
     * @param fichConf l'adresse du fichier de configuration
     * @param g une instance de GestionnaireErreur
     */
    private static void conv1to2 (Properties conf, File fichConf, GestionnaireErreur g) {
    
		g.afficheMessage(" conversion 1 vers 2...", Nat.LOG_DEBUG);					
		g.afficheMessage(" ... ajout des nouvelles clés et valeurs",  Nat.LOG_DEBUG);					
		conf.setProperty(OptNames.fi_infos,"infos à saisir");
		conf.setProperty(OptNames.fi_name,"conf récupérée \""+fichConf.getName()+"\"");
		conf.setProperty(OptNames.conf_version, "2");
		conf.setProperty(OptNames.fi_is_sys_config,"false");
	
		g.afficheMessage(" ... enregistrement dans le répertoire utilisateur", Nat.LOG_DEBUG);					
		try {
			conf.store(new FileOutputStream(ConfigNat.getUserConfigFilterFolder()+fichConf.getName()), null);
			g.afficheMessage("OK !", Nat.LOG_DEBUG);
			g.afficheMessage("tentative d'effacement du vieux fichier...", Nat.LOG_DEBUG);
			if (fichConf.delete()) {g.afficheMessage("OK !", Nat.LOG_DEBUG);}
			else {g.afficheMessage("Impossible, évitez d'utiliser cette conf.", Nat.LOG_DEBUG);}
			//chaineok += nomConf + "\n";
		}
		catch (IOException ioer) {
			ioer.printStackTrace(); 
			g.afficheMessage("Erreur enregistrement/effacement pour la config version 1"+fichConf.getName(),Nat.LOG_DEBUG);
			g.afficheMessage(ioer.getLocalizedMessage(), Nat.LOG_DEBUG);
			//chaineko += allFiles[i] + "\n";
		}
		catch (SecurityException se) {
			g.afficheMessage(se.getLocalizedMessage(), Nat.LOG_DEBUG);
		}
		
	}
    
    /**
     * Change une configuration version 2 en configuration version 3
     * @param conf l'instance de Properties représenant la configuration
     * @param fichConf l'adresse du fichier de configuration
     * @param g une instance de GestionnaireErreur
     */
    private static void conv2to3 (Properties conf, File fichConf, GestionnaireErreur g) {
    	g.afficheMessage(" conversion 2 vers 3...", Nat.LOG_DEBUG);					
		g.afficheMessage(" ... ajout/suppression des nouvelles clés et valeurs",  Nat.LOG_DEBUG);
		conf.setProperty(OptNames.conf_version, "3");
		conf.remove(OptNames.fi_dtd_filename);
		conf.remove(OptNames.fi_filter_filename);
		conf.remove(OptNames.fi_litt_fr_abbreg_filter_filename);
		conf.remove(OptNames.fi_litt_fr_int_filter_filename);
		conf.remove(OptNames.fi_math_filter_filename);
		if (!(conf.containsKey(OptNames.fi_is_sys_braille_table)))
				{conf.setProperty(OptNames.fi_is_sys_braille_table, "true");}
		if (!(conf.containsKey(OptNames.fi_is_sys_emboss_table)))
				{conf.setProperty(OptNames.fi_is_sys_emboss_table, "true");}
		String bt = conf.getProperty(OptNames.fi_braille_table, "TbFr2007.ent");
		if (!(bt.endsWith(".ent"))) {bt=bt+".ent";}
		String et = conf.getProperty(OptNames.pr_emboss_table, "codeUS.ent");
		if (!(et.endsWith(".ent"))) {et=et+".ent";}
		conf.setProperty(OptNames.fi_braille_table,bt);
		conf.setProperty(OptNames.pr_emboss_table,et);
		g.afficheMessage(" ... enregistrement dans le répertoire utilisateur", Nat.LOG_DEBUG);					
		try {
			conf.store(new FileOutputStream(fichConf), null);
			g.afficheMessage("OK !", Nat.LOG_DEBUG);
		}
		catch (IOException ioer) {
			ioer.printStackTrace(); 
			g.afficheMessage("Erreur d'écriture pour la config version 2"+fichConf.getName(),Nat.LOG_DEBUG);
			g.afficheMessage(ioer.getLocalizedMessage(), Nat.LOG_DEBUG);
			//chaineko += allFiles[i] + "\n";
		}
		
    }
}

