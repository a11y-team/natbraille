/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

/**
 * Inutilisé pour l'instant
 * Contiendra les éléments de configuration détaillés pour la conversion
 */
public class ConfConversion extends OngletConf
{
	/** Pour la sérialisation (non utilisé)*/
	private static final long serialVersionUID = 1L;
	
	/** Constructeur*/
	public ConfConversion(){super();}
	/**
	 * Renvoie toujours true
	 * @return true
	 * @see ui.SavableTabbedConfigurationPane#enregistrer()
	 */
	public boolean enregistrer() {return true;}
	/**
	 * Renvoie toujours true
	 * @return true
	 * @see ui.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	public boolean enregistrer(String f){return true;}
}