/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Collections;
import java.util.Vector;

import nat.ConfigNat;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
//import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import outils.FileToolKit;
/**
 * Onglet de configuration de l'embossage (ou de l'impression)
 * @author bruno
 *
 */
public class ConfEmbossage extends OngletConf implements ItemListener
{
	/** Pour la sérialisation (non utilisé)*/
	private static final long serialVersionUID = 1L;
	/** JLabel pour {@link #tfCommande}*/
	private JLabel lCommande = new JLabel("Commande impression:");
	/** Textfield contenant la commande d'embossage*/
	private JTextField tfCommande = new JTextField(20);
	/** JLabel pour {@link #jcbTablesEmb}*/
	private JLabel ljcbTable = new JLabel("Table Braille pour l'embossage:");
	/** JComboBox contenant les tables braille possibles pour l'embossage*/
	private JComboBox jcbTablesEmb = new JComboBox();
	/** Case à cocher pour l'embossage par commande système */
	private JCheckBox jchbCommande = new JCheckBox("Utiliser une commande système pour l'embossage");
	/** Case à cocher pour embossage par périphérique installé */
	private JCheckBox jchbEmbosseuse = new JCheckBox("Utiliser directement un périphérique d'embossage");
	/** JLabel pour {@link #jcbEmbosseuse}*/
	private JLabel lEmbosseuse = new JLabel("Choix du périphérique:");
	/** Liste des périphériques installés sur la machine*/
	private JComboBox jcbEmbosseuse = new JComboBox();
	
	/** 
	 * Constructeur
	 * <p>Fabrique l'onglet et initialise les composants graphiques</p>
	 */
	public ConfEmbossage()
	{
		super();
		getAccessibleContext().setAccessibleDescription("Activez cet onglet pour afficher les options d'embossage");
		getAccessibleContext().setAccessibleName("Onglet contenant les options d'embossage");
		
		/*
		 * Préparation des composants
		 */
		
		/* Table embossage braille */
		/* Liste Tables braille */
		File repertoire =new File("xsl/tablesEmbosseuse");
		File[] listTablesSys = repertoire.listFiles();
		File rep2 = new File(ConfigNat.getUserEmbossTableFolder());
		File[] listTablesUser = rep2.listFiles();
	    
		int tailleSys = listTablesSys.length;
		int tailleUser = listTablesUser.length;
		Vector<BrailleTableListItem> namesList = new Vector<BrailleTableListItem>(tailleSys + tailleUser);
		
		for(int i=0;i<tailleSys;i++)
	    {
			String nomTable = FileToolKit.getSysDepPath(listTablesSys[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Embtab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,true));
	    	}
	    }
		for(int i=0;i<tailleUser;i++)
	    {
			String nomTable = FileToolKit.getSysDepPath(listTablesUser[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Embtab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,false));
	    	}
	    }
		Collections.sort(namesList);
		
		jcbTablesEmb  = new JComboBox(namesList);
		String selectedTablePath = ConfigNat.getUserEmbossTableFolder();
		if(ConfigNat.getCurrentConfig().getIsSysEmbossTable()){selectedTablePath = ConfigNat.getInstallFolder()+"xsl/tablesEmbosseuse/";}
		jcbTablesEmb.setSelectedItem(new BrailleTableListItem(selectedTablePath+ConfigNat.getCurrentConfig().getTableEmbossage(),
				ConfigNat.getCurrentConfig().getIsSysEmbossTable()));
		jcbTablesEmb.setRenderer(new BrailleTableComboBoxRenderer());
		jcbTablesEmb.setEditable(false);
	    jcbTablesEmb.getAccessibleContext().setAccessibleName("Liste à choix multiples table braille");
	    jcbTablesEmb.getAccessibleContext().setAccessibleDescription("Sélectionner à l'aide des flèches la table braille à utiliser pour l'embossage");
	    jcbTablesEmb.setToolTipText("Sélectionner la table braille d'embossage (Alt+t)");

	    ljcbTable.setLabelFor(jcbTablesEmb);
	    ljcbTable.setDisplayedMnemonic('t');
	    
	    jchbCommande.getAccessibleContext().setAccessibleName("Case à cocher utilisation d'une commande système pour l'embossage");
	    jchbCommande.getAccessibleContext().setAccessibleDescription("Cochez cette case si vous souhaitez embosser à l'aide d'une commande");
	    jchbCommande.setToolTipText("Cochez cette case si vous souhaitez embosser à l'aide d'une commande (Alt+c)");
	    jchbCommande.setSelected(ConfigNat.getCurrentConfig().getUtiliserCommandeEmbossage());
	    jchbCommande.setMnemonic('c');
	    
	    jchbCommande.addItemListener(this);
	  
	    lCommande.setLabelFor(tfCommande);
	    lCommande.setDisplayedMnemonic('i');
	    tfCommande.getAccessibleContext().setAccessibleName("Zone de texte commande pour l'embossage");
	    tfCommande.getAccessibleContext().setAccessibleDescription("Entrez la commande à exécuter pour lancer l'embossage");
	    tfCommande.setToolTipText("Entrez la commande à exécuter pour lancer l'embossage (Alt+i)");
	    tfCommande.setText(ConfigNat.getCurrentConfig().getCommande());
	    
	    if(!jchbCommande.isSelected())
	    {
	    	lCommande.setEnabled(false);
	    	tfCommande.setEnabled(false);
	    }
	    else
	    {
	    	jchbEmbosseuse.setSelected(false);
	    }
	    
	    jchbEmbosseuse.getAccessibleContext().setAccessibleName("Case à cocher utilisation directe d'une embosseuse");
	    jchbEmbosseuse.getAccessibleContext().setAccessibleDescription("Cochez cette case si vous souhaitez embosser directement à l'aide d'une embosseuse installée");
	    jchbEmbosseuse.setToolTipText("Cochez cette case si vous souhaitez embosser à l'aide d'une embosseuse installée (Alt+e)");
	    jchbEmbosseuse.setMnemonic('e');
	    jchbEmbosseuse.addItemListener(this);
	    jchbEmbosseuse.setSelected(ConfigNat.getCurrentConfig().getUtiliserEmbosseuse());
	  
	    lEmbosseuse.setLabelFor(jcbEmbosseuse);
	    lEmbosseuse.setDisplayedMnemonic('p');
	    jcbEmbosseuse.getAccessibleContext().setAccessibleName("Zone de texte commande pour l'embossage");
	    jcbEmbosseuse.getAccessibleContext().setAccessibleDescription("Entrez la commande à exécuter pour lancer l'embossage");
	    jcbEmbosseuse.setToolTipText("Entrez la commande à exécuter pour lancer l'embossage (Alt+p)");
	    //jcbEmbosseuse.setText(confNat.getembosseuse());
	    setListePrinters();
	    
	    if(!jchbEmbosseuse.isSelected())
	    {
	    	jcbEmbosseuse.setEnabled(false);
	    	lEmbosseuse.setEnabled(false);
	    }
	    else
	    {
	    	jchbCommande.setSelected(false);
	    }
		/*
		 * Mise en page
		 */
		
		JPanel pEmbo = new JPanel();
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		pEmbo.setLayout(gbl);
		
		JLabel titre = new JLabel("<html><h3>Configuration de l'embossage</h3></html>");
		
		gbc.anchor = GridBagConstraints.WEST;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbl.setConstraints(titre, gbc);
		pEmbo.add(titre);
		
		gbc.anchor = GridBagConstraints.WEST;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbl.setConstraints(ljcbTable, gbc);
		pEmbo.add(ljcbTable);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbl.setConstraints(jcbTablesEmb, gbc);
		pEmbo.add(jcbTablesEmb);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		gbl.setConstraints(jchbCommande, gbc);
		pEmbo.add(jchbCommande);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.anchor = GridBagConstraints.EAST;
		gbl.setConstraints(lCommande, gbc);
		pEmbo.add(lCommande);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.WEST;
		gbl.setConstraints(tfCommande, gbc);
		pEmbo.add(tfCommande);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 2;
		gbl.setConstraints(jchbEmbosseuse, gbc);
		pEmbo.add(jchbEmbosseuse);
		
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 1;
		gbc.anchor = GridBagConstraints.EAST;
		gbl.setConstraints(lEmbosseuse, gbc);
		pEmbo.add(lEmbosseuse);
		
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.anchor = GridBagConstraints.WEST;
		gbl.setConstraints(jcbEmbosseuse, gbc);
		pEmbo.add(jcbEmbosseuse);
		/*pEmbo.add(lOs);
		pEmbo.add(jcbOs);*/
		add(pEmbo);
	}
	/**
	 * Initialisation de {@link #jcbEmbosseuse} avec les différents PrintServices du système
	 */
	private void setListePrinters() 
	{
		//on récupère les imprimantes et leur docFlavors
		PrintService[] pservices = PrintServiceLookup.lookupPrintServices(null, null);
		//il y a des imprimantes?
		jcbEmbosseuse.setEnabled(false);
		for (int i = 0; i < pservices.length; i++)
		{
			//System.err.println(pservices[i]);
			if(pservices[i]!= null)
			{
				jcbEmbosseuse.addItem(pservices[i].getName());
				if (ConfigNat.getCurrentConfig().getPrintservice()!= null && pservices[i].getName().compareTo(ConfigNat.getCurrentConfig().getPrintservice())==0)
				{
					jcbEmbosseuse.setSelectedIndex(i);
				}
				jcbEmbosseuse.setEnabled(true);
			}
		}
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer()
	 */
	public boolean enregistrer()
	{
		boolean retour = true;
		ConfigNat.getCurrentConfig().setTableEmbossage(jcbTablesEmb.getSelectedItem().toString(),
				((BrailleTableListItem)jcbTablesEmb.getSelectedItem()).getIsSystem());
		ConfigNat.getCurrentConfig().setIsSysEmbossTable(((BrailleTableListItem)jcbTablesEmb.getSelectedItem()).getIsSystem());
		//confNat.setOs((String)jcbOs.getSelectedItem());
		ConfigNat.getCurrentConfig().setCommande(tfCommande.getText());
		ConfigNat.getCurrentConfig().setUtiliserCommandeEmbossage(jchbCommande.isSelected());
		ConfigNat.getCurrentConfig().setUtiliserEmbosseuse(jchbEmbosseuse.isSelected());
		ConfigNat.getCurrentConfig().setPrintService((String)jcbEmbosseuse.getSelectedItem());
		//ConfigNat.getCurrentConfig().Sauvegarder();
		return retour;
	}
	/**
	 * Implémentation de <code>ItemListener</code>
	 * <p>Active (resp. désactive) les options d'embossage via ligne de commande (resp. périphérique)
	 * et inversement</p>
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	public void itemStateChanged(ItemEvent e)
	{
		if (e.getSource()==jchbCommande)
		{
		    if(!jchbCommande.isSelected())
		    {
		    	lCommande.setEnabled(false);
		    	tfCommande.setEnabled(false);
		    }
		    else
		    {
		    	lCommande.setEnabled(true);
		    	tfCommande.setEnabled(true);
		    	jchbEmbosseuse.setSelected(false);
		    }
		}
		else if (e.getSource()==jchbEmbosseuse)
		{
		    if(!jchbEmbosseuse.isSelected())
		    {
		    	lEmbosseuse.setEnabled(false);
		    	jcbEmbosseuse.setEnabled(false);
		    }
		    else
		    {
		    	lEmbosseuse.setEnabled(true);
		    	jcbEmbosseuse.setEnabled(true);
		    	jchbCommande.setSelected(false);
		    	//dispatchEvent(new ItemEvent(jchbCommande, ItemEvent.DESELECTED, jchbCommande, ItemEvent.DESELECTED));
		    }
		}	
	}
	/** Méthode d'accès en lecture à {@link #jcbTablesEmb}
	 * @return {@link #jcbTablesEmb}
	 **/
	public JComboBox getComboTables(){return jcbTablesEmb;}
}
