/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.SortedMap;
import java.util.Vector;

import nat.ConfigNat;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import outils.FileToolKit;
/**
 * Onglet de configuration générale (principales options)
 * @see OngletConf
 * @author bruno
 *
 */
public class ConfGeneral extends OngletConf implements ActionListener, ItemListener
{
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;

	/** Champ d'information sur la configuration */
	private JTextField tfInfos= new JTextField(20);
	/** JCheckBox activer braille abrégé */
	private JCheckBox abreger = new JCheckBox("Braille abrégé (partiel)");
	/** JCheckBox activer braille littéraire */
	private JCheckBox bLit = new JCheckBox("Traiter les écritures littéraires");
	/** JCheckBox activer braille mathématique */
	private JCheckBox bMaths = new JCheckBox("Traiter les écritures mathématiques");
	/** JCheckBox activer braille musical */
	private JCheckBox bMusique = new JCheckBox("<html>Traiter les écritures m<u>u</u>sicales. <b color=\"red\">Ne pas utiliser: en développement</b></html>");
	/** Liste des tables brailles */
	private JComboBox comboTables;
	/** label pour la liste des tables braille */
	private JLabel lComboTables = new JLabel("Table Braille");
	/** bouton édition de la table braille */
	private JButton btEditTable =new JButton("Editer",new ImageIcon("ui/icon/gtk-edit.png"));
	/** liste des encodages possibles de la source */
	private JComboBox jcbCharsetSource = new JComboBox();
	/** label pour la liste des encodages possibles de la source */
	private JLabel ljcbCharsetSource = new JLabel("Encodage document en noir");
	/** liste des encodages possibles de la sortie */
	private JComboBox jcbCharsetSortie = new JComboBox();
	/** label pour la liste des encodages possibles de la sortie */
	private JLabel ljcbCharsetSortie = new JLabel("Encodage document braille");
	/** bouton pour l'édition des règles d'abrégé */
	private JButton btEditAbr = new JButton("Choisir les règles",new ImageIcon("ui/icon/gtk-edit.png"));
	
	/**
	 * Active la mise en page (dont la coupure mathématique)
	 * Remplace jcbCoupure
	 * @since 2.0
	 */
	private JCheckBox jcbMEP = new JCheckBox("Activer la mise en page");
	/**
	 * Active ou non la coupure
	 * @deprecated 2.0
	 * @see #jcbMEP
	 */
	@Deprecated
	private JCheckBox jcbCoupure = new JCheckBox("Activer la coupure");
	/** JCheckbox activer la coupure littéraire */
	private JCheckBox jcbCoupureLit = new JCheckBox("Activer la coupure littéraire");
	/** Bouton éditer les règles de coupure littéraire */
	private JButton btEditCoup =new JButton("Editer les règles",new ImageIcon("ui/icon/gtk-edit.png"));
	/** JCheckBox activer le mode sagouin pour la coupure */
	private JCheckBox jcbSagouin = new JCheckBox("Activer le mode sagouin");
	/** liste des niveaux de verbosité possibles */ 
	private JComboBox cbLog;
	/** label pour la liste des niveaux de verbosité possibles */
	private JLabel lLog = new JLabel("Niveau de verbosité");
	/** L'instance de la fenêtre Configuration contenant l'onglet */ 
	private Configuration jfParent;
	
	/**
	 * Constructeur
	 * @param jfp la fenêtre parente Configuration 
	 */
	public ConfGeneral(Configuration jfp)
	{
		super();
		getAccessibleContext().setAccessibleDescription("Activez cet onglet pour afficher les options principales");
		getAccessibleContext().setAccessibleName("Onglet contenant les options principales");
		this.jfParent = jfp;
		
		/**********
		 * Préparation des composants
		 */
		
		JLabel titreInfos = new JLabel("<html><h3><u>D</u>escription</h3></html>");
		titreInfos.setLabelFor(tfInfos);
		titreInfos.setDisplayedMnemonic('d');
		
		tfInfos.getAccessibleContext().setAccessibleName("Zone de texte description de la configuration");
		//tfInfos.getAccessibleContext().setAccessibleDescription("Saisir ici le texte de description de la configuration");
		tfInfos.setToolTipText("Modifier la description de la configuration (Alt+d)");
		tfInfos.setText(ConfigNat.getCurrentConfig().getInfos());
				
		bLit.setSelected(ConfigNat.getCurrentConfig().getTraiterLiteraire());
		//bLit.getAccessibleContext().setAccessibleName("Braille littéraire");
		//bLit.getAccessibleContext().setAccessibleDescription("Pour activer le traitement des contenus littéraires");
		bLit.setToolTipText("Activer le traitement des contenus littéraires (textes) (Atl+l)");
		bLit.setMnemonic('l');
		
		bMaths.setSelected(ConfigNat.getCurrentConfig().getTraiterMaths());
		//bMaths.getAccessibleContext().setAccessibleName("Braille mathématique");
		//bMaths.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer le traitement des contenus mathématiques");
		bMaths.setToolTipText("Activer le traitement des contenus mathématiques (formules, mathml) (Alt+m)");
		bMaths.setMnemonic('m');
		
		bMusique.setSelected(ConfigNat.getCurrentConfig().getTraiterMusique());
		//bMusique.getAccessibleContext().setAccessibleName("Case à cocher braille musical");
		//bMusique.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer le traitement des contenus musicaux");
		bMusique.setToolTipText("Activer le traitement des contenus musicaux (Alt+u)");
		bMusique.setMnemonic('u');

		
		abreger.setSelected(ConfigNat.getCurrentConfig().getAbreger());
		//abreger.getAccessibleContext().setAccessibleName("Case à cocher braille abrégé");
		//abreger.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer l'abrégé");
		abreger.setToolTipText("Activer le braille abrégé (Alt+a)");
		abreger.setMnemonic('a');
		
		btEditAbr.addActionListener(this);
		btEditAbr.getAccessibleContext().setAccessibleName("Choisir les règles d'abrégé");
		//btEditAbr.getAccessibleContext().setAccessibleDescription("valider pour choisir les règles d'abrégé");
		btEditAbr.setToolTipText("Pour sélectionner une à une les règles d'abrégé (Alt+h)");
		btEditAbr.setMnemonic('h');

		/* Liste Tables braille */
		File repertoire =new File("xsl/tablesBraille");
		File[] listTablesSys = repertoire.listFiles();
		File rep2 = new File(ConfigNat.getUserBrailleTableFolder());
		File[] listTablesUser = rep2.listFiles();
	    
		int tailleSys = listTablesSys.length;
		int tailleUser = listTablesUser.length;
		
		Vector<BrailleTableListItem> namesList = new Vector<BrailleTableListItem>(tailleSys + tailleUser);
		
		for(int i=0;i<tailleSys;i++)
	    {
			String nomTable = FileToolKit.getSysDepPath(listTablesSys[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Brltab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,true));
	    	}
	    }
		for(int i=0;i<tailleUser;i++)
	    {
			String nomTable = FileToolKit.getSysDepPath(listTablesUser[i].getAbsolutePath()) ;
	    	if (nomTable.endsWith(".ent") && !nomTable.endsWith("Brltab.ent"))
	    	{
	    		//namesList.add(nomTable.substring(0,nomTable.length()-4));
	    		namesList.add(new BrailleTableListItem(nomTable,false));
	    	}
	    }
		
		Collections.sort(namesList);
		
		comboTables  = new JComboBox(namesList);
		String selectedTablePath = ConfigNat.getUserBrailleTableFolder();
		if(ConfigNat.getCurrentConfig().getIsSysTable()){selectedTablePath = ConfigNat.getInstallFolder()+"xsl/tablesBraille/";}
		comboTables.setSelectedItem(new BrailleTableListItem(selectedTablePath+ConfigNat.getCurrentConfig().getTableBraille(),
				ConfigNat.getCurrentConfig().getIsSysTable()));
		comboTables.setRenderer(new BrailleTableComboBoxRenderer());
	    comboTables.setEditable(false);
	    
	    /* Encodage */
	    //ajout de la détection automatique
		jcbCharsetSource.addItem("automatique");
		jcbCharsetSortie.addItem("automatique");
		//récupération des charsets disponibles
		SortedMap<String,Charset> charsets = Charset.availableCharsets();
		for(String nom : charsets.keySet())
		{
			jcbCharsetSource.addItem(nom);
			jcbCharsetSortie.addItem(nom);

    		if(nom.equals(ConfigNat.getCurrentConfig().getNoirEncoding()))
    		{
    			jcbCharsetSource.setSelectedIndex(jcbCharsetSource.getItemCount()-1);
    		}
    		if(nom.equals(ConfigNat.getCurrentConfig().getBrailleEncoding()))
    		{
    			jcbCharsetSortie.setSelectedIndex(jcbCharsetSortie.getItemCount()-1);
    		}
		}
		//jcbCharsetSource.getAccessibleContext().setAccessibleName("Liste à choix multiples encodage source ");
		//jcbCharsetSource.getAccessibleContext().setAccessibleDescription("Sélectionner l'encodage du fichier source, ou laisser la détection automatique");
		jcbCharsetSource.setToolTipText("Sélectionner l'encodage du fichier en noir, ou laisser la détection automatique (Alt+e)");
		
		//jcbCharsetSortie.getAccessibleContext().setAccessibleName("Liste à choix multiples encodage sortie ");
		//jcbCharsetSortie.getAccessibleContext().setAccessibleDescription("Sélectionner l'encodage du fichier sortie, ou laisser la détection automatique");
		jcbCharsetSortie.setToolTipText("Sélectionner l'encodage du fichier en braille, ou laisser la détection automatique (Alt+o)");

		lComboTables.setLabelFor(comboTables);
		lComboTables.setDisplayedMnemonic('b');
		
		btEditTable.addActionListener(this);
		btEditTable.getAccessibleContext().setAccessibleName("Editer la table braille");
		//btEditTable.getAccessibleContext().setAccessibleDescription("valider pour éditer la table braille");
		btEditTable.setToolTipText("Editer la table braille sélectionnée (Alt+t)");
		btEditTable.setMnemonic('t');
		
		ljcbCharsetSource.setLabelFor(jcbCharsetSource);
		ljcbCharsetSource.setDisplayedMnemonic('e');
		
		//comboTables.getAccessibleContext().setAccessibleName("Liste à choix multiples table braille");
		//comboTables.getAccessibleContext().setAccessibleDescription("Sélectionner à l'aide des flèches la table braille à utiliser");
		comboTables.setToolTipText("Choix de la table braille (Alt+b)");
		
		ljcbCharsetSortie.setLabelFor(jcbCharsetSortie);
		ljcbCharsetSortie.setDisplayedMnemonic('o');
		
		jcbMEP.setSelected(ConfigNat.getCurrentConfig().getMep());
		//jcbMEP.getAccessibleContext().setAccessibleName("Case à cocher mise en page");
		//jcbMEP.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer la mise en page");
		jcbMEP.setToolTipText("Activer la mise en page (Alt+i)");
		jcbMEP.setMnemonic('i');	
		jcbMEP.addItemListener(this);
		jcbMEP.addActionListener(this.jfParent);
		
		/*jcbCoupure.setSelected(ConfigNat.getCurrentConfig().getCoupure());
		jcbCoupure.getAccessibleContext().setAccessibleName("Case à cocher coupure");
		jcbCoupure.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer la coupure");
		jcbCoupure.setToolTipText("Activer la coupure");
		jcbCoupure.setMnemonic('');	
		jcbCoupure.addItemListener(this);*/
		
		jcbCoupureLit.setSelected(ConfigNat.getCurrentConfig().getCoupureLit());
		//jcbCoupureLit.getAccessibleContext().setAccessibleName("Case à cocher coupure littéraire");
		//jcbCoupureLit.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer la coupure littéraire");
		jcbCoupureLit.setToolTipText("Activer la coupure littéraire des mots (Alt+p)");
		jcbCoupureLit.setMnemonic('p');
		
		btEditCoup.addActionListener(this);
		btEditCoup.getAccessibleContext().setAccessibleName("Règles de césure");
		//btEditCoup.getAccessibleContext().setAccessibleDescription("valider pour éditer les règles de coupure des mots");
		btEditCoup.setToolTipText("Editer les règles de césure des mots (Alt+r)");
		btEditCoup.setMnemonic('r');
		
		jcbSagouin.setSelected(ConfigNat.getCurrentConfig().getModeCoupureSagouin());
		//jcbSagouin.getAccessibleContext().setAccessibleName("Case à cocher mode sagouin coupure");
		//jcbSagouin.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer la coupure en mode sagouin");
		jcbSagouin.setToolTipText("Activer la coupure en mode sagouin (Alt+c)");
		jcbSagouin.setMnemonic('c');
		
		//if (!jcbCoupure.isSelected()){jcbSagouin.setEnabled(false);}
		if (!jcbMEP.isSelected())
		{
			jcbSagouin.setEnabled(false);
			jcbCoupureLit.setEnabled(false);
		}
		
		// verbosité des logs:
		String [] listeModeVerbeux = {"faible","moyen","élevé","debugage"};
		cbLog = new JComboBox(listeModeVerbeux);
		//cbLog.getAccessibleContext().setAccessibleName("Liste à choix multiples niveau de verbosité des messages");
		//cbLog.getAccessibleContext().setAccessibleDescription("Sélectionner le niveau de verbosité des messages");
		cbLog.setToolTipText("Sélectionner le niveau de verbosité des messages, présentés du plus faible au plus élevé (débugage) (Alt+v)");
		cbLog.setSelectedIndex(ConfigNat.getCurrentConfig().getNiveauLog()-1);

		lLog.setLabelFor(cbLog);
		lLog.setDisplayedMnemonic('v');
		
		
		/* ********
		 * Mise en page
		 */
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		JPanel panGen = new JPanel(gbl);
		
		gbc.anchor = GridBagConstraints.WEST;
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		
		gbl.setConstraints(titreInfos, gbc);
		panGen.add(titreInfos);
		
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(tfInfos, gbc);
		panGen.add(tfInfos);
		
		gbc.gridx=0;
		gbc.gridy++;
		JLabel titreEcritures = new JLabel("<html><h3>Ecritures brailles</h3></html>");
		gbl.setConstraints(titreEcritures, gbc);
		panGen.add(titreEcritures);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(bLit, gbc);
		panGen.add(bLit);
		
		gbc.gridy++;
		gbl.setConstraints(bMaths, gbc);
		panGen.add(bMaths);
		
		gbc.gridy++;
		gbl.setConstraints(bMusique, gbc);
		panGen.add(bMusique);
		
		gbc.gridy++;
		gbc.gridwidth=  1;
		gbl.setConstraints(abreger, gbc);
		panGen.add(abreger);
		
		gbc.gridx++;
		gbl.setConstraints(btEditAbr, gbc);
		panGen.add(btEditAbr);
		
		JLabel titreEncodage = new JLabel("<html><h3>Encodage des fichiers et table braille</h3></html>");
		gbc.gridy++;
		gbc.gridx=0;
		gbc.gridwidth = 3;
		gbl.setConstraints(titreEncodage, gbc);
		panGen.add(titreEncodage);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth = 1;
		gbl.setConstraints(lComboTables, gbc);
		panGen.add(lComboTables);
		gbc.gridx++;
		gbl.setConstraints(comboTables, gbc);
		panGen.add(comboTables);
		gbc.gridx++;
		gbl.setConstraints(btEditTable, gbc);
		panGen.add(btEditTable);

		gbc.gridy++;
		gbc.gridx = 1;
		gbl.setConstraints(ljcbCharsetSource, gbc);
		panGen.add(ljcbCharsetSource);
		gbc.gridx++;
		gbl.setConstraints(jcbCharsetSource, gbc);
		panGen.add(jcbCharsetSource);
		
		gbc.gridy++;
		gbc.gridx = 1;
		gbl.setConstraints(ljcbCharsetSortie, gbc);
		panGen.add(ljcbCharsetSortie);
		gbc.gridx++;
		gbl.setConstraints(jcbCharsetSortie, gbc);
		panGen.add(jcbCharsetSortie);
	
		gbc.gridx = 0;
		gbc.gridy++;
		gbc.gridwidth = 1;
		JLabel titreCoupure = new JLabel("<html><h3>Mise en page</h3></html>");
		gbl.setConstraints(titreCoupure, gbc);
		panGen.add(titreCoupure);
		
		gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(jcbMEP, gbc);
		panGen.add(jcbMEP);
		
		/*gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(jcbCoupure, gbc);
		add(jcbCoupure);*/
		
		gbc.gridy++;
		gbl.setConstraints(jcbSagouin, gbc);
		panGen.add(jcbSagouin);
		
		gbc.gridy++;
		gbl.setConstraints(jcbCoupureLit, gbc);
		panGen.add(jcbCoupureLit);
		
		gbc.gridx++;
		gbl.setConstraints(btEditCoup, gbc);
		panGen.add(btEditCoup);
		
		gbc.gridx = 0;
		gbc.gridy++;
		gbc.gridwidth = 3;
		JLabel titreLog = new JLabel("<html><h3>Messages</h3></html>");
		gbl.setConstraints(titreLog, gbc);
		panGen.add(titreLog);
		
		gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy++;
		gbl.setConstraints(lLog, gbc);
		panGen.add(lLog);
		gbc.gridx++;
		gbl.setConstraints(cbLog, gbc);
		panGen.add(cbLog);
		
		setLayout(new BorderLayout());
		add(panGen,BorderLayout.NORTH);
	}
	
	/**
	 * Redéfinie de ActionListener
	 * Ouvre les éditions de la table braille ou des règles de coupure
	 * @param evt L'instance d'ActionEvent
	 */
	public void actionPerformed(ActionEvent evt)
	{
		if (evt.getSource()==btEditTable)
		{
			ConfTableBraille ctb = new ConfTableBraille((BrailleTableListItem)comboTables.getSelectedItem(), jfParent.getGmtb());
			ctb.setVisible(true);
		}
		else if (evt.getSource()==btEditCoup)
		{
			ConfDictCoup cdc = new ConfDictCoup();
			cdc.setVisible(true);
		}
		else if (evt.getSource()==btEditAbr)
		{
			new ConfAbrege();
		}
	}
	/**
	 * Redéfinie de ItemListener
	 * Active ou désactive les options de mise en page suivant la valeur de {@link #jcbMEP}
	 * @param e l'ItemEvent
	 */
	public void itemStateChanged(ItemEvent e)
	{
		if(e.getSource()==jcbMEP)
		{
			if(!jcbMEP.isSelected())
			{
				jcbSagouin.setEnabled(false);
				jcbCoupureLit.setEnabled(false);
			}
			else
			{
				jcbSagouin.setEnabled(true);
				jcbCoupureLit.setEnabled(true);
			}
		}
	}
	/**
	 * Enregistre les options de l'onglet(non-Javadoc)
	 * @see ui.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer()
	 */
	public boolean enregistrer()
	{
		boolean retour = true;
		try
		{
			ConfigNat.getCurrentConfig().setInfos(tfInfos.getText());
			ConfigNat.getCurrentConfig().setTableBraille(comboTables.getSelectedItem().toString(),
					((BrailleTableListItem)comboTables.getSelectedItem()).getIsSystem());
			ConfigNat.getCurrentConfig().setIsSysTable(((BrailleTableListItem)comboTables.getSelectedItem()).getIsSystem());
			ConfigNat.getCurrentConfig().setAbreger(abreger.isSelected());
			ConfigNat.getCurrentConfig().setTraiterLiteraire(bLit.isSelected());
			ConfigNat.getCurrentConfig().setTraiterMaths(bMaths.isSelected());
			ConfigNat.getCurrentConfig().setTraiterMusique(bMusique.isSelected());
			ConfigNat.getCurrentConfig().setNoirEncoding((String)jcbCharsetSource.getSelectedItem());
			ConfigNat.getCurrentConfig().setBrailleEncoding((String)jcbCharsetSortie.getSelectedItem());
			ConfigNat.getCurrentConfig().setNiveauLog(cbLog.getSelectedIndex()+1,jfParent.getGestErreur());
			ConfigNat.getCurrentConfig().setMep(jcbMEP.isSelected());
			ConfigNat.getCurrentConfig().setCoupure(jcbCoupure.isSelected());
			ConfigNat.getCurrentConfig().setCoupureLit(jcbCoupureLit.isSelected());
			ConfigNat.getCurrentConfig().setModeCoupureSagouin(jcbSagouin.isSelected());
		}
		catch(Exception e){e.printStackTrace();retour=false;}
		
		return retour;
	}
	/**
	 * Méthode d'accès
	 * @return le JComboBox {@link #comboTables}
	 */
	public JComboBox getComboTables(){return comboTables;}
	/**
	 * Méthode d'accès
	 * @return le JCheckBox {@link #jcbMEP}
	 */
	public JCheckBox getMepBox() {return jcbMEP;}
}
