/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import nat.ConfigNat;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
/**
* Onglet de configuration de l'interface graphique
* @see OngletConf
* @author bruno
*
*/
public class ConfInterface extends OngletConf implements ItemListener, ActionListener
{
	/** pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	/** Constante d'accès représentant l'indice de "éditeur intégré" dans {@link #jcbEditeur}*/
	private final int EDITEUR_NAT = 0;
	/** Constante d'accès représentant l'indice de "éditeur par défaut" dans {@link #jcbEditeur}*/
	private final int EDITEUR_DEFAUT = 1;
	/** Constante d'accès représentant l'indice de "autre application" dans {@link #jcbEditeur}*/
	private final int EDITEUR_AUTRE = 2;
	/** JComboBox conteannt la liste des types d'éditeurs possibles*/
	private JComboBox jcbEditeur;
	/** Label pour jtfEditeur
	 * @see ConfInterface#jtfEditeur
	 */
	private JLabel ljtfEditeur = new JLabel("Application externe pour l'édition:");
	/** JtextField contenant l'adresse de l'application d'édition*/
	private JTextField jtfEditeur = new JTextField(10);
	/** Bouton ouvrant le JFileChooser permettant de choisir l'application */
	JButton jbtChoixEdit = new JButton("parcourir...");
	/* JFileChooser pour le choix de l'application /
	private JFileChooser selectionneAppli = new JFileChooser();*/
	/** JCheckBox ouvrir automatiquement l'éditeur après la transcription */
	private JCheckBox editeurBraille = new JCheckBox("Ouvrir automatiquement l'éditeur après la transcription");
	//private JCheckBox editeurEnBraille = new JCheckBox("Editeur en Braille");
	/** Label pour cbPolice
	 * @see ConfInterface#cbPolice
	 */
	private JLabel lCbPolice = new JLabel("Police de l'éditeur:");
	/** Liste des polices pour la police principale de l'éditeur */
	private JComboBox cbPolice;
	/** Label pour jsTaillePolice
	 * @see ConfInterface#jsTaillePolice
	 */
	private JLabel lTaillePolice = new JLabel("Taille de la police:");
	/** JSpinner pour la taille de la police principale de l'éditeur */
	private JSpinner jsTaillePolice;// = new JTextField(3);
	/** Case à cocher pour l'affichage de la ligne secondaire dans l'éditeur */
	private JCheckBox afficheLigne = new JCheckBox("Affiche la ligne secondaire");
	/** Label pour cbPolice2
	 * @see ConfInterface#cbPolice2
	 */
	private JLabel lCbPolice2 = new JLabel("Police de la ligne secondaire:");
	/** Liste des polices pour la police secondaire de l'éditeur */
	private JComboBox cbPolice2;
	/** Label pour lTaillePolice2
	 * @see ConfInterface#lTaillePolice2
	 */
	private JLabel lTaillePolice2 = new JLabel("Taille de la police secondaire:");
	/** JSpinner pour la taille de la police secondaire de l'éditeur */
	private JSpinner jsTaillePolice2;// = new JTextField(3);
	
	//private Configuration jfParent;
	/** case à cocher pour la mémorisation des dimensions des fenêtres */
	private JCheckBox jcbTailleFenetre = new JCheckBox("Mémoriser les dimensions des fenêtres principale, d'options et l'éditeur");
	/** case à cocher pour le centrage des fenêtres à l'écran */
	private JCheckBox jcbCentrerFenetre = new JCheckBox("Centrer les fenêtres à l'écran");
	/** case à cocher pour jouer un son périodiquement pendant la transcription */
	private JCheckBox jcbSonPendantTrans = new JCheckBox("Alerte sonores pendant la transcription");
	/** case à cocher pour jouer un son à la fin de la transcription */
	private JCheckBox jcbSonFin = new JCheckBox("Alerte sonores à la fin de la transcription");
	/** case à cocher pour nommer automatiquement le fichier de sortie */
	private JCheckBox jcbSortieAuto = new JCheckBox("Nom du fichier de sortie automatique");
	
	/** Constructeur */
	public ConfInterface()
	{
		super();
		getAccessibleContext().setAccessibleDescription("Activez cet onglet pour afficher les options de l'interface graphique");
		getAccessibleContext().setAccessibleName("Onglet contenant les options de l'interface graphique");
		//this.jfParent = jfp;
		
		/*
		 * Choix de l'éditeur
		 */
		//Construction de jcbEditeur
		String[] listeEdit = {"l'éditeur intégré","l'éditeur par défaut", "une application externe"};
		jcbEditeur = new JComboBox(listeEdit);
		jcbEditeur.getAccessibleContext().setAccessibleName("Liste déroulante choix du type d'éditeur");
		jcbEditeur.getAccessibleContext().setAccessibleDescription("Utiliser les flèches haut-bas pour choisir le type d'éditeur souhaité");
		jcbEditeur.setToolTipText("Sélectionnez dans la liste le type d'éditeur désiré (Alt+e)");
		jcbEditeur.addItemListener(this);
		
		//initiailisation
		activeEditeurExterne(false);
		if(ConfigNat.getCurrentConfig().getUseNatEditor()){jcbEditeur.setSelectedIndex(EDITEUR_NAT);}
		else if(ConfigNat.getCurrentConfig().getUseDefaultEditor()){jcbEditeur.setSelectedIndex(EDITEUR_DEFAUT);}
		else
		{
			jcbEditeur.setSelectedIndex(EDITEUR_AUTRE);
			activeEditeurExterne(true);
		}
		
		JLabel ljcbEditeur = new JLabel("Utiliser comme éditeur");
		ljcbEditeur.setLabelFor(jcbEditeur);
		ljcbEditeur.setDisplayedMnemonic('e');
		
		ljtfEditeur.setLabelFor(jtfEditeur);
		ljtfEditeur.setDisplayedMnemonic('x');
		
		jtfEditeur.setText(ConfigNat.getCurrentConfig().getEditeur());
		jtfEditeur.getAccessibleContext().setAccessibleDescription("Entrez l'adresse de l'application externe d'édition");
		jtfEditeur.getAccessibleContext().setAccessibleName("Zone de texte adresse de l'application externe");
		jtfEditeur.setToolTipText("Adresse de l'application externe (Alt+x)");
		
		jbtChoixEdit.addActionListener(this);
		jbtChoixEdit.getAccessibleContext().setAccessibleName("Bouton parcourir");
		jbtChoixEdit.getAccessibleContext().setAccessibleDescription("Valider pour rechercher l'application externe dans le système de fichier");
		jbtChoixEdit.setToolTipText("Recherche de l'application externe dans le système de fichier (Alt+r)");
		jbtChoixEdit.setMnemonic('r');
		
		editeurBraille.setSelected(ConfigNat.getCurrentConfig().getOuvrirEditeur());
		editeurBraille.getAccessibleContext().setAccessibleName("Case à cocher ouvrir l'éditeur");
		editeurBraille.getAccessibleContext().setAccessibleDescription("Cocher la case pour ouvrir automatiquement l'éditeur après une transcription");
		editeurBraille.setToolTipText("Ouvrir l'éditeur après la transcription (Alt+i)");
		editeurBraille.setMnemonic('i');		
		
		/*
		 * Option de l'éditeur intégré
		 */
		GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		/** Liste de toutes les polices : */
		//Font[ ] polices = environment.getAllFonts();
		/** Liste des noms de toutes les polices : */
		String[ ] nomPolices = environment.getAvailableFontFamilyNames();
		cbPolice = new JComboBox(nomPolices);
		cbPolice.getAccessibleContext().setAccessibleName("Liste à choix multiples police de l'éditeur ");
		cbPolice.getAccessibleContext().setAccessibleDescription("Sélectionner la police de l'éditeur intégré");
		cbPolice.setToolTipText("Sélectionner la police à utiliser avec l'éditeur intégré (Alt+u)");
		
		lCbPolice.setLabelFor(cbPolice);
		lCbPolice.setDisplayedMnemonic('u');

		cbPolice.setSelectedItem(ConfigNat.getCurrentConfig().getPoliceEditeur());
		
		jsTaillePolice = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getTaillePolice(), 1, 300, 1));
		jsTaillePolice.getAccessibleContext().setAccessibleName("Champ texte taille de la police");
		jsTaillePolice.getAccessibleContext().setAccessibleDescription("Sélectionner la taille de la police de l'éditeur");
		jsTaillePolice.setToolTipText("Sélectionner la taille de police à utiliser avec l'éditeur (Alt+t)");
		
		lTaillePolice.setLabelFor(jsTaillePolice);
		lTaillePolice.setDisplayedMnemonic('t');
		//jtfTaillePolice.setText(ConfigNat.getCurrentConfig().getTaillePolice() + "");
		
		afficheLigne.setSelected(ConfigNat.getCurrentConfig().getAfficheLigneSecondaire());
		afficheLigne.getAccessibleContext().setAccessibleName("Case à cocher afficher la ligne secondaire de l'éditeur");
		afficheLigne.getAccessibleContext().setAccessibleDescription("Cocher la case pour afficher la ligne secondaire dans l'éditeur");
		afficheLigne.setToolTipText("Afficher la ligne secondaire de l'éditeur (Alt+d)");
		afficheLigne.setMnemonic('d');
		afficheLigne.addItemListener(this);
		
		cbPolice2 = new JComboBox(nomPolices);
		cbPolice2.getAccessibleContext().setAccessibleName("Liste à choix multiples police secondaire ");
		cbPolice2.getAccessibleContext().setAccessibleDescription("Sélectionner la police secondaire de l'éditeur intégré");
		cbPolice2.setToolTipText("Sélectionner la police secondaire à utiliser avec l'éditeur intégré (Alt+o)");
		
		lCbPolice2.setLabelFor(cbPolice2);
		lCbPolice2.setDisplayedMnemonic('o');
		cbPolice2.setSelectedItem(ConfigNat.getCurrentConfig().getPolice2Editeur());
		
		jsTaillePolice2 = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getTaillePolice2(), 1, 300, 1));
		jsTaillePolice2.getAccessibleContext().setAccessibleName("Liste de valeurs de taille de la police secondaire");
		jsTaillePolice2.getAccessibleContext().setAccessibleDescription("Sélectionner à l'aide des flèches la taille de la police secondaire de l'éditeur");
		jsTaillePolice2.setToolTipText("Sélectionner la taille de police secondaire à utiliser (Alt+a)");
		
		lTaillePolice2.setLabelFor(jsTaillePolice2);
		lTaillePolice2.setDisplayedMnemonic('a');
		//jtfTaillePolice2.setText(ConfigNat.getCurrentConfig().getTaillePolice2() + "");
		
		if (!afficheLigne.isSelected())
		{
			lCbPolice2.setEnabled(false);
			cbPolice2.setEnabled(false);
			jsTaillePolice2.setEnabled(false);
			lTaillePolice2.setEnabled(false);
		}
		
		
		/* options valables pour toute l'interface quelle que soit la conf */
		jcbTailleFenetre.setSelected(ConfigNat.getCurrentConfig().getMemoriserFenetre());
		jcbTailleFenetre.getAccessibleContext().setAccessibleName("Case à cocher mémoriser les tailles des fenêtres");
		jcbTailleFenetre.getAccessibleContext().setAccessibleDescription("Cocher la case pour mémoriser les tailles de fenêtre");
		jcbTailleFenetre.setToolTipText("Mémoriser la taille des fenêtres lors de redimensionnement (Alt+m)");
		jcbTailleFenetre.setMnemonic('m');
		
		jcbCentrerFenetre.setSelected(ConfigNat.getCurrentConfig().getCentrerFenetre());
		jcbCentrerFenetre.getAccessibleContext().setAccessibleName("Case à cocher centrer les fenêtres");
		jcbCentrerFenetre.getAccessibleContext().setAccessibleDescription("Cocher la case pour centrer les fenêtres à l'écran");
		jcbCentrerFenetre.setToolTipText("Centrer les fenêtres à l'écran (Alt+c)");
		jcbCentrerFenetre.setMnemonic('c');
		
		jcbSonPendantTrans.setSelected(ConfigNat.getCurrentConfig().getSonPendantTranscription());
		jcbSonPendantTrans.getAccessibleContext().setAccessibleName("Case à cocher jouer un son pendant la transcription");
		jcbSonPendantTrans.getAccessibleContext().setAccessibleDescription("Cocher la case pour qu'un son soit joué à intervalles réguliers pendant la transcription");
		jcbSonPendantTrans.setToolTipText("Jouer un son à intervalles réguliers pendant la transcription (Alt+p)");
		jcbSonPendantTrans.setMnemonic('p');
		
		jcbSonFin.setSelected(ConfigNat.getCurrentConfig().getSonFinTranscription());
		jcbSonFin.getAccessibleContext().setAccessibleName("Case à cocher jouer un son à la fin de la transcription");
		jcbSonFin.getAccessibleContext().setAccessibleDescription("Cocher la case pour qu'un son soit joué à la fin de la transcription");
		jcbSonFin.setToolTipText("Jouer un son à la fin de la transcription (Alt+f)");
		jcbSonFin.setMnemonic('f');
		
		jcbSortieAuto.setSelected(ConfigNat.getCurrentConfig().getSortieAuto());
		jcbSortieAuto.getAccessibleContext().setAccessibleName("Case à cocher nom de fichier de sortie automatique");
		jcbSortieAuto.getAccessibleContext().setAccessibleDescription("Cocher la case pour que le fichier de sortie soit nommé automatiquement à partir du fichier d'entrée");
		jcbSortieAuto.setToolTipText("Nommer automatiquement le fichier de sortie (Alt+q)");
		jcbSortieAuto.setMnemonic('q');
		
		/*
		 * Mise en page
		 */
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		//setLayout(gbl);
		
		JPanel p = new JPanel();
		p.setLayout(gbl);
		
		gbc.anchor = GridBagConstraints.WEST;
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		JLabel titre = new JLabel("<html><h3>Options générales de l'éditeur</h3></html>");
		gbl.setConstraints(titre, gbc);
		p.add(titre);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(ljcbEditeur, gbc);
		p.add(ljcbEditeur);
		
		gbc.gridx++;
		gbl.setConstraints(jcbEditeur, gbc);
		p.add(jcbEditeur);
		
		gbc.gridx=1;
		gbc.gridwidth = 1;
		gbc.gridy++;
		gbl.setConstraints(ljtfEditeur, gbc);
		p.add(ljtfEditeur);
		
		gbc.gridx++;
		gbl.setConstraints(jtfEditeur, gbc);
		p.add(jtfEditeur);
		
		gbc.gridx++;
		gbl.setConstraints(jbtChoixEdit, gbc);
		p.add(jbtChoixEdit);
		
		gbc.gridx=1;
		gbc.gridwidth = 3;
		gbc.gridy++;
		gbl.setConstraints(editeurBraille, gbc);
		p.add(editeurBraille);
		
		gbc.gridwidth = 3;
		gbc.insets = new Insets(3,3,3,3);
		gbc.gridx=0;
		gbc.gridy++;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		JLabel titreEditeur = new JLabel("<html><h3>Editeur intégré</h3></html>");
		gbl.setConstraints(titreEditeur, gbc);
		p.add(titreEditeur);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(lCbPolice, gbc);
		p.add(lCbPolice);
		gbc.gridx++;
		gbl.setConstraints(cbPolice, gbc);
		p.add(cbPolice);
		
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(lTaillePolice, gbc);
		p.add(lTaillePolice);
		gbc.gridx++;
		gbl.setConstraints(jsTaillePolice, gbc);
		p.add(jsTaillePolice);
		
		gbc.gridwidth = 2;
		gbc.gridy++;
		gbc.gridx = 1;
		gbl.setConstraints(afficheLigne, gbc);
		p.add(afficheLigne);
		
		gbc.gridwidth = 1;
		gbc.gridy++;
		gbl.setConstraints(lCbPolice2, gbc);
		p.add(lCbPolice2);
		gbc.gridx=2;
		gbl.setConstraints(cbPolice2, gbc);
		p.add(cbPolice2);
		
		gbc.gridx = 1;
		gbc.gridy++;
		gbl.setConstraints(lTaillePolice2, gbc);
		p.add(lTaillePolice2);
		gbc.gridx++;
		gbl.setConstraints(jsTaillePolice2, gbc);
		p.add(jsTaillePolice2);
		
		JLabel lTitreGeneral = new JLabel("<html><h3>Options générales</h3></html>");
		gbc.gridx = 0;
		gbc.insets = new Insets(3,3,3,3);
		gbc.gridwidth = 3;
		gbc.gridy++;
		gbl.setConstraints(lTitreGeneral, gbc);
		p.add(lTitreGeneral);
		
		JLabel lInfo = new JLabel("Les options suivantes s'appliquent à l'interface quelle que soit la configuration en cours");
		gbc.gridwidth = 4;
		gbc.gridy++;
		gbl.setConstraints(lInfo, gbc);
		p.add(lInfo);
		
		gbc.insets = new Insets(3,30,3,3);
		gbc.gridwidth = 2;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(jcbTailleFenetre, gbc);
		p.add(jcbTailleFenetre);
		
		gbc.gridy++;
		gbl.setConstraints(jcbCentrerFenetre, gbc);
		p.add(jcbCentrerFenetre);
		
		gbc.gridy++;
		gbl.setConstraints(jcbSonFin, gbc);
		p.add(jcbSonFin);
		
		gbc.gridy++;
		gbl.setConstraints(jcbSonPendantTrans, gbc);
		p.add(jcbSonPendantTrans);
		
		gbc.gridy++;
		gbl.setConstraints(jcbSortieAuto, gbc);
		p.add(jcbSortieAuto);
		
		add(p);
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer()
	 */
	public boolean enregistrer()
	{
		boolean retour = true;
		try
		{
			ConfigNat.getCurrentConfig().setPoliceEditeur((String)cbPolice.getSelectedItem());
			ConfigNat.getCurrentConfig().setOuvreEditeurApresTranscription(editeurBraille.isSelected());
			ConfigNat.getCurrentConfig().setTaillePolice(((Integer)jsTaillePolice.getValue()).intValue());
			ConfigNat.getCurrentConfig().setAfficheLigneSecondaire(afficheLigne.isSelected());
			ConfigNat.getCurrentConfig().setTaillePolice2(((Integer)jsTaillePolice2.getValue()).intValue());
			ConfigNat.getCurrentConfig().setPolice2Editeur((String)cbPolice2.getSelectedItem());
			ConfigNat.getCurrentConfig().setCentrerFenetre(jcbCentrerFenetre.isSelected());
			ConfigNat.getCurrentConfig().setMemoriserFenetre(jcbTailleFenetre.isSelected());
			ConfigNat.getCurrentConfig().setSonFinTranscription(jcbSonFin.isSelected());
			ConfigNat.getCurrentConfig().setSonPendantTranscription(jcbSonPendantTrans.isSelected());
			ConfigNat.getCurrentConfig().setSortieAuto(jcbSortieAuto.isSelected());
			switch(jcbEditeur.getSelectedIndex())
			{
				case(EDITEUR_NAT):
					ConfigNat.getCurrentConfig().setUseNatEditor(true);
					ConfigNat.getCurrentConfig().setUseDefaultEditor(false);
					break;
				case(EDITEUR_DEFAUT):
					ConfigNat.getCurrentConfig().setUseNatEditor(false);
					ConfigNat.getCurrentConfig().setUseDefaultEditor(true);
					break;
				case(EDITEUR_AUTRE):
					ConfigNat.getCurrentConfig().setUseNatEditor(false);
					ConfigNat.getCurrentConfig().setUseDefaultEditor(false);
					break;	
			}
			ConfigNat.getCurrentConfig().setEditeur(jtfEditeur.getText());
		}
		/*
		catch (NumberFormatException nbe)
		{
			/*JOptionPane jopErreur = new JOptionPane();
			jopErreur.showMessageDialog( jfParent,"Vous devez entrer un entier pour la longueur de la ligne et la taille de police","Erreur de saisie", jopErreur.ERROR_MESSAGE);*/
			/*JOptionPane.showMessageDialog(jfParent,"Vous devez entrer un entier pour la longueur de la ligne et la taille de police","Erreur de saisie", JOptionPane.ERROR_MESSAGE);
			retour = false;
		}*/
		catch (Exception e){e.printStackTrace();retour=false;}
			
		return retour;
	}
	/**
	 * Implémentation de ItemListener
	 * Active ou désactive les options concernant la ligne secondaire suivant la valeur de afficheLigne
	 * @param ie l'ItemEvent généré
	 * @see ConfInterface#afficheLigne
	 * @see  ConfInterface#lCbPolice2
	 * @see  ConfInterface#cbPolice2
	 * @see  ConfInterface#lTaillePolice2
	 */
	public void itemStateChanged(ItemEvent ie) 
	{
		if(ie.getSource()==afficheLigne)
	    {
			if(!afficheLigne.isSelected())
		    {
		    	lCbPolice2.setEnabled(false);
				cbPolice2.setEnabled(false);
				jsTaillePolice2.setEnabled(false);
				lTaillePolice2.setEnabled(false);
		    }
		    else
		    {
		    	lCbPolice2.setEnabled(true);
				cbPolice2.setEnabled(true);
				jsTaillePolice2.setEnabled(true);
				lTaillePolice2.setEnabled(true);
		    }
	    }
		else if(ie.getSource()==jcbEditeur)
		{
			if(jcbEditeur.getSelectedIndex()==EDITEUR_AUTRE){activeEditeurExterne(true);}
			else{activeEditeurExterne(false);}
		}
		
	}
	
	/**
	 * Modifie les composant de configuration en fonction de b
	 * @param b true si utilisation d'un éditeur externe
	 */
	private void activeEditeurExterne(boolean b)
	{
		jtfEditeur.setEnabled(b);
		ljtfEditeur.setEnabled(b);
		jbtChoixEdit.setEnabled(b);
	}
	/**
	 * Implémentation de ActionListener
	 * Ouvre un JFileChooser pour le choix de l'application externe de l'édition
	 * Remplit le JTextField associé
	 * @param ae ActionEvent généré
	 * @see ConfInterface#jtfEditeur
	 * @see ConfInterface#jbtChoixEdit
	 */
	public void actionPerformed(ActionEvent ae)
	{
		JFileChooser selectionneAppli = new JFileChooser();
		selectionneAppli.setDialogTitle("Sélection de l'application externe pour l'édition");
 		if (selectionneAppli.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
         {    
 			//si un fichier est selectionné, récupérer le fichier puis son path
 			jtfEditeur.setText(selectionneAppli.getSelectedFile().getAbsolutePath()); 
         }
	}
}