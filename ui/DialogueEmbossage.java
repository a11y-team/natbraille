/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * Dialogue permettant la sélection des pages à imprimer
 * @author bruno
 *
 */
public class DialogueEmbossage extends JDialog implements ActionListener
{

	/** pour la sérialisation (ne sert pas)*/
	private static final long serialVersionUID = 1L;
	/** Bouton radio sélectionner toutes les pages */
	private JRadioButton jrbToutes = new JRadioButton("Toutes les pages");
	/** Bouton radio sélectionner la page courante */
	private JRadioButton jrbActu = new JRadioButton();
	/** Bouton radio sélection particulière des pages */
	private JRadioButton jrbSpec = new JRadioButton("Uniquement les pages:");
	/** JTextField sélection des pages */
	private JTextField jtfPages = new JTextField(8);
	/** bouton embosser */
	private JButton jbEmbosser = new JButton("Embosser");
	/** bouton annuler */
	private JButton jbAnnuler = new JButton("Annuler");
	
	/** la page actuellement affichée dans l'éditeur */
	private int pageActu =0;
	/** tableau des pages */
	private boolean[] pages;
	
	
	/**
	 * Constructeur
	 * @param parent La fenêtre appelante
	 * @param p tableau d'entiers qui contiendra les pages à imprimer
	 * @param pActu page actuelle
	 */
	public DialogueEmbossage(JFrame parent, boolean[] p, int pActu)
	{
		super(parent, "Embossage");
		setModal(true);
		pages = p;
		pageActu = pActu;
		fabriqueFenetre();
		setVisible(true);
	}

	/**
	 * Fabrique la fenêtre de dialogue
	 */
	private void fabriqueFenetre()
	{		
		jrbActu.setText("Page actuelle (" + (pageActu + 1) + ")");
		ButtonGroup bg = new ButtonGroup();
		bg.add(jrbToutes);
		bg.add(jrbActu);
		bg.add(jrbSpec);
		
		jrbToutes.setSelected(true);
		jrbToutes.setMnemonic('t');
		jrbToutes.getAccessibleContext().setAccessibleName("Case d'option embosser toutes les pages");
		jrbToutes.getAccessibleContext().setAccessibleDescription("Sélectionnez cette option pour embosser toutes les pages (alt+t)");
		jrbToutes.setToolTipText("Embossage de toutes les pages (alt+t)");
		

		jrbActu.setMnemonic('p');
		jrbActu.getAccessibleContext().setAccessibleName("Case d'option embosser la page actuelle");
		jrbActu.getAccessibleContext().setAccessibleDescription("Sélectionnez cette option pour embosser la page actuellement affichée dans l'éditeur (alt+p)");
		jrbActu.setToolTipText("Embossage de la page actuellement affichée dans l'éditeur (alt+p)");
		

		jrbSpec.setMnemonic('u');
		jrbSpec.getAccessibleContext().setAccessibleName("Case d'option embosser uniquement les pages sélectionnées");
		jrbSpec.getAccessibleContext().setAccessibleDescription("Sélectionnez cette option pour embosser uniquement les pages sélectionnées (alt+u)");
		jrbSpec.setToolTipText("Embosse les pages sélectionnées (alt+u)");
		
		jbEmbosser.addActionListener(this);
		jbEmbosser.setMnemonic('e');
		jbEmbosser.getAccessibleContext().setAccessibleName("Bouton embosser");
		jbEmbosser.getAccessibleContext().setAccessibleDescription("Valider pour lancer l'embossage (alt+e)");
		jbEmbosser.setToolTipText("Valider pour lancer l'embossage (alt+e)");
		
		jbAnnuler.addActionListener(this);
		jbAnnuler.setMnemonic('a');
		jbAnnuler.getAccessibleContext().setAccessibleName("Bouton annuler");
		jbAnnuler.getAccessibleContext().setAccessibleDescription("Valider pour anuuler l'embossage et revenir à l'éditeur (alt+a)");
		jbAnnuler.setToolTipText("Valider pour anuuler l'embossage et revenir à l'éditeur (alt+a)");
		
		jtfPages.setToolTipText("Entrez les pages à imprimer, éventuellement sous forme de plages (2-5 par exemple), et séparées par des ; ");
		jtfPages.getAccessibleContext().setAccessibleName("Champ texte pour la sélection des pages à imprimer");
		jtfPages.getAccessibleContext().setAccessibleDescription("Entrez les pages à imprimer, éventuellement sous forme de plages (2-5 par exemple), et séparées par des ; ");
		
		JLabel lJtfPages = new JLabel("<html>Syntaxe pour la sélection des pages: <br>" +
			" <i>sous forme de plage (ex: 2-5) et/ou par numéro,<br>" +
			" séparés par des ; (ex: 2-5; 7; 8-10)</i></html>");
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		JLabel lTitre = new JLabel("Pages à embosser:");

		c.gridx=0;
		c.gridy=0;
		c.insets = new Insets(3,3,3,3);
		c.anchor = GridBagConstraints.WEST;
		add(lTitre,c);
		
		c.gridy++;
		add(jrbToutes,c);
		
		c.gridy++;
		add(jrbActu,c);
		
		c.gridy++;
		add(jrbSpec,c);
		c.gridx++;
		add(jtfPages,c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=2;
		add(lJtfPages,c);
		
		c.gridy++;
		c.gridwidth=1;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(10,3,3,3);
		add(jbEmbosser,c);
		c.gridx++;
		add(jbAnnuler,c);
		
		pack();
		
	}

	/**
	 * Gère les actions sur les boutons {@link #jbAnnuler} et {@link #jbEmbosser}
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==jbEmbosser)
		{
			majPages();
			dispose();
		}
		else if (ae.getSource()==jbAnnuler)
		{
			//mise à false
			for(int i=0;i<pages.length;i++){pages[i]=false;}
			dispose();
		}
	}

	/**
	 * Met à jour {@link #pages}; si une page n'est pas sélectionnée, mets -1 comme valeur
	 */
	private void majPages()
	{
		if(jrbActu.isSelected())
		{
			int i=0;
			for(i=0; i<pageActu;i++){pages[i]=false;}
			pages[i]=true;
			for(i=pageActu+1;i<pages.length;i++){pages[i]=false;}
		}
		else if(jrbToutes.isSelected())
		{
			for(int i=0;i<pages.length;i++){pages[i]=true;}
		}
		else if(jrbSpec.isSelected())
		{
			//initialisation à false
			for(int i=0;i<pages.length;i++){pages[i]=false;}
			
			//parsage de la chaine des motifs
			String [] motifs = jtfPages.getText().split(";");
			for(int i =0; i<motifs.length;i++)
			{
				String [] sousMotifs= motifs[i].split("-");
				if (sousMotifs.length == 1) //un seul nombre
				{
					int p = -1;
					try{p = Integer.parseInt(sousMotifs[0]) - 1;}
					catch(NumberFormatException nfe){nfe.printStackTrace();}//erreur de conversion
					if(p>-1 && p < pages.length){pages[p]=true;}
				}
				if (sousMotifs.length == 2)
				{
					int p1 =-1, p2 = -1;
					try
					{
						p1 = Integer.parseInt(sousMotifs[0]) - 1;
						p2 = Integer.parseInt(sousMotifs[1]) - 1;
					}
					catch(NumberFormatException nfe){nfe.printStackTrace();}//erreur de conversion
					if(p1>-1 && p1 < pages.length && p2>-1 && p2 < pages.length && p1<=p2)
					{
						for(int j= p1; j<= p2;j++){pages[j]=true;}
					}
				}
			}
		}
		
	}

}
