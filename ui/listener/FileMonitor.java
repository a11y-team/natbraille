/*
 * Trace assistant
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.listener;

/**
 * @author bruno
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Réalise le monitoring d'un fichiers
 * @author bruno
 *
 */
public class FileMonitor {

    /** instance de la fabrique de FileMonitor*/
    private static final FileMonitor fm = new FileMonitor();

    /** Timer pour le temps de vérification des modifs sur le fichier*/
    private Timer timer;
    /** Entrées du timer */
    private Hashtable <String,FileMonitorTask>timerEntries;

    /** 
     * renvoie une instance de FileMonitor
     * @return instance de la fabrique
     */
    public static FileMonitor getInstance() {return fm;}

    /**
     * Constructeur (protected)
     */
    protected FileMonitor()
    { 
        //crée et lance le timer
    	timer = new Timer(true);
    	timerEntries = new Hashtable<String,FileMonitorTask>();
    }
    
    /** Add a monitored file with a FileChangeListener.
     * @param listener listener to notify when the file changed.
     * @param fileName name of the file to monitor.
     * @param period polling period in milliseconds.
     * @throws FileNotFoundException si le fichier en paramètre n'existe pas
     */
    public void addFileChangeListener(FileChangeListener listener, String fileName, long period) throws FileNotFoundException
    {
		removeFileChangeListener(listener, fileName);
		FileMonitorTask task = new FileMonitorTask(listener, fileName);
		timerEntries.put(fileName + listener.hashCode(), task);
		timer.schedule(task, period, period);
    }

    /** Remove the listener from the notification list.
     * @param listener the listener to be removed.
     * @param fileName le nom du fichier lié au listener
     */
    public void removeFileChangeListener(FileChangeListener listener, String fileName)
    {
        FileMonitorTask task = timerEntries.remove(fileName + listener.hashCode());
        if (task != null){task.cancel();}
    }

    /**
     * Déclencheur
     * @param listener le listener
     * @param fileName nom du fichier
     */
    protected void fireFileChangeEvent(FileChangeListener listener, String fileName) {listener.fileChanged(fileName);}

    /**
     * Classe interne décrivant une tâche à accomplir pour le file monitor
     * @author bruno
     *
     */
    class FileMonitorTask extends TimerTask
    {
        /** Le listener de fichier*/
        FileChangeListener listener;
        /** Nom du fichier*/
        String fileName;
        /** Fichier suivi*/
        File monitoredFile;
        /** date de la dernière modif*/
        long lastModified;

        /**
         * Constructeur
         * @param lis le file change listener
         * @param fName le nom du fichier
         * @throws FileNotFoundException si le fichier n'existe pas
         */
        public FileMonitorTask(FileChangeListener lis, String fName) throws FileNotFoundException
        {
		    listener = lis;
		    fileName = fName;
		    lastModified = 0;
	
		    monitoredFile = new File(fileName);
		    if (!monitoredFile.exists())
		    { 
		    	URL fileURL = listener.getClass().getClassLoader().getResource(fileName);
				if (fileURL != null) {monitoredFile = new File(fileURL.getFile());}
				else {throw new FileNotFoundException("File Not Found: "+ fileName);}
		    }
		    lastModified = monitoredFile.lastModified();
        }

        /**
         * Vérifie l'état des modification du fichier
         * @see java.util.TimerTask#run()
         */
        @Override
        public void run()
        {
		    long lm = monitoredFile.lastModified();
		    if (lm != lastModified)
		    {
		        lastModified = lm;
		        fireFileChangeEvent(this.listener, this.fileName);
		    }
        }
    }
}

