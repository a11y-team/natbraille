<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:functx='http://www.functx.com'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:xhtml='http://www.w3.org/1999/xhtml'
xmlns:nat='http://natbraille.free.fr/xsl'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>
	
	<xsl:import href="functions/functx-1.1alpha.xsl" /> <!-- functx functions -->
	<xsl:import href="nat-functs.xsl" />
	<xsl:include href="outils.xsl"/>
	<!-- <xsl:include href="hyphenation.xsl"/> -->

	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:variable name="l_maj" as="xs:string">ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜÁÍÓÚÑÌÒÄÖ&Ccedil;&AElig;&OElig;</xsl:variable>
	<xsl:variable name="l_maj_A" as="xs:string">AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</xsl:variable>
	<xsl:variable name="l_min" as="xs:string">abcdefghijklmnopqrstuvwxyzàâéèêëîïôùûüáíóúñìòäö&cedil;&aelig;&oelig;</xsl:variable>
	<xsl:variable name="l_min_a" as="xs:string">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</xsl:variable>
	<xsl:variable name="l_num" as="xs:string">0123456789+-×÷=,.&sup2;&sup3;</xsl:variable>
	<xsl:variable name="l_amb" as="xs:string">ÂÊÎÔÛËÏÜŒWâêîôûëïüœ"</xsl:variable>
	<xsl:variable name="l_amb_ahat" as="xs:string">ââââââââââââââââââââ</xsl:variable>
	<xsl:variable name="l_alphabet" as="xs:string">abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
	<xsl:variable name="l_chiffres" as="xs:string">1234567890,.</xsl:variable>
	<xsl:variable name="l_grec_min" as="xs:string">&alpha;&beta;&gamma;&delta;&epsi;&epsiv;&zeta;&eta;&theta;&thetav;&iota;&kappa;&lambda;&mu;&nu;&xi;&omicron;&pi;&piv;&rho;&rhov;&sigma;&sigmav;&tau;&upsilon;&phi;&phiv;&chi;&psi;&omega;</xsl:variable>
	<xsl:variable name="l_grec_maj" as="xs:string">&Agr;&Bgr;&Gamma;&Delta;&Egr;&Zgr;&EEgr;&Theta;&Igr;&Kgr;&Lambda;&Mgr;&Ngr;&Xi;&Ogr;&Pi;&Rgr;&Sigma;&Tgr;&Upsilon;&Phi;&KHgr;&Chi;&Psi;&Omega;</xsl:variable>
	<!--<xsl:variable name="l_alphanumgrec" select="fn:concat($l_alphabet,$l_chiffres,$l_grec_min,$l_grec_maj)" />-->
	<xsl:variable name="l_alphanumgrec" as="xs:string" select="concat($l_alphabet,$l_chiffres,$l_grec_min,$l_grec_maj)" />
	<!-- Bruno: SUPER IMPORTANT: ne pas placer cette variable en tête de liste des variables, je sais pas pourquoi mais elle fait planter des fois le parsage de la feuille-->
	<xsl:variable name="chaine_vide" as="xs:string" select="''"/>
	<xsl:strip-space elements = "tableau col ligne lit phrase math li ol ul tr th td vide" />

	<xsl:template match="doc:doc">
		<doc>
			<xsl:apply-templates select="@*|*|text()|processing-instruction()" />
		</doc>
	</xsl:template>

	<xsl:template match="page-break">
		<xsl:copy/>
	</xsl:template>

	<xsl:template match="echap">
		<xsl:value-of select="."/>
	</xsl:template>
	
	<xsl:template name="rempli">
		<xsl:param name="nb" select="-10" as="xs:integer"/>
		<xsl:param name="motif" as="xs:string"/>
		<xsl:if test="$nb &gt; 0">
			<xsl:value-of select="$motif"/>
			<xsl:call-template name="rempli">
				<xsl:with-param name="nb" select="$nb - 1"/>
				<xsl:with-param name="motif" select="$motif"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="ol|ul|li"> <!-- listes -->
		<xsl:copy>
			<xsl:copy-of select="@*" /> <!-- recopie des attributs -->
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="titre|phrase|tableau">
		<xsl:variable name="exprTmp" as="xs:string*">
			<xsl:apply-templates/>
		</xsl:variable>
		<xsl:variable name="expr" as="xs:string">
			<xsl:value-of select="functx:trim(translate(string-join($exprTmp,''),'&#10;&#9;',''))"/>
			<!-- <xsl:value-of select="string-join($exprTmp,'')"/> -->
		</xsl:variable>

		<xsl:variable name="phrasePretePourMep">
			<xsl:choose>
				<!-- il y a une table mathématique dans l'expression -->
				<xsl:when test="contains($expr,$debTable)"><!-- and $linearise_table">-->
					<xsl:call-template name="mepTables">
						<xsl:with-param name="expr" select="$expr"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!-- on applique directement les templates -->
					<xsl:value-of select="$expr"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
	
		<xsl:copy>
			<xsl:if test="self::titre">
				<xsl:variable name="nivBra" as="xs:integer" select="@niveauOrig" />
				<xsl:attribute name="niveauBraille" select="$listeTitres[$nivBra]" />
				<!-- changement des niveaux de titre entre original et braille -->
			</xsl:if>
			<!-- si la phrase n'est pas vide (càd avec autre chose que des espaces) on la garde -->
			<xsl:if test="not(translate(string($phrasePretePourMep),concat($espace,'&pt;'),'')='')">
				<xsl:value-of select="$phrasePretePourMep"/>
			</xsl:if>
		</xsl:copy>
	</xsl:template>

	<xsl:template name="mepTables">
		<xsl:param name="expr" as="xs:string"/>
		<xsl:param name="premier" select="0" as="xs:integer"/>
		<!-- on commence par traiter ce qu'il y a avant la table -->
		<xsl:variable name="avantSeq" as="xs:string*">
			<xsl:value-of select="substring-before($expr,$debTable)"/>
		</xsl:variable>
		<!-- affichage de ce qu'il y a avant avec remplacements des marques pour les espaces et les sauts -->
		<xsl:variable name="avant" as="xs:string" select="string-join($avantSeq,'')"/>
		<xsl:value-of select="$avant"/>
		<!-- la table -->
		<xsl:variable name="table" as="xs:string">
			<xsl:value-of select="substring-before(substring-after($expr,$debTable),$finTable)"/>
		</xsl:variable>
		<xsl:variable name="lesLignes" as="xs:string*" select="tokenize(translate($table,$carcoup,''),$sautAGenerer)"/>

		<!-- hypothèse: si il n'y a pas de saut de lignes, c'est que c'est linéarisé-->
		<xsl:variable name="laTable" as="xs:string*">
			<xsl:for-each select="$lesLignes">
				<xsl:value-of select="translate(.,$carcoup,'')"/>
				<xsl:if test="not(position()=last())">
					<xsl:value-of select="$sautAGenerer"/>
				</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<!-- affichage de la table -->
		<xsl:value-of select="$debTable"/>
		<!--<xsl:value-of select="translate(string-join($laTable,''),concat($espace,$sautAGenerer),'&pt;&#10;')"/>-->
		<xsl:value-of select="string-join($laTable,'')" />
		<xsl:value-of select="$finTable"/>		
		<xsl:variable name="apres">
			<xsl:value-of select="substring-after($expr,$finTable)"/>
		</xsl:variable>
		<xsl:choose>
			<!-- y a-t-il encore une table après?-->
			<xsl:when test="contains($apres,$debTable)">
				<!--<xsl:value-of select="concat($avant,string-join($table,''))"/>-->
				<xsl:call-template name="mepTables">
					<xsl:with-param name="expr" select="$apres"/>
					<xsl:with-param name="premier" select="0"/>
				</xsl:call-template>
			</xsl:when>
			<!-- on traite ce qu'il y a après la table -->
			<xsl:otherwise><!-- pas de table ni de coupure dans ce qui suit -->
				<xsl:value-of select="$apres"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="tableau[@type='lit']">
		<xsl:apply-templates mode="ligne" select="."/>
	</xsl:template>

	<xsl:template match="tableau" mode="ligne">
		<xsl:variable name="nbCol" select="count(tr[1]/th)" as="xs:integer"/><!-- nombre de colonnes du tableau -->
		<!-- transcription des données du tableau -->
		<!-- entètes -->
		<xsl:variable name="entetes" as="xs:string*">
			<xsl:for-each select="tr[child::th]">
				<xsl:for-each select="th">
					<xsl:variable name="resu">
						<xsl:apply-templates/>
					</xsl:variable>
					<xsl:value-of select="$resu"/><!--concat($resu,if (not(position() mod ($nbCol) =0)) then $espace else ())"/>-->
				</xsl:for-each>
			</xsl:for-each>
		</xsl:variable>
		<!-- corps du tableau -->
		<xsl:variable name="nblEnt" select="count(tr[child::th])" as="xs:integer"/><!-- nombre de lignes d'entête -->
		<!--<xsl:message select="$nblEnt"/>
		<xsl:message select="$nbCol"/>-->
		<xsl:variable name="corps" as="xs:string*">
			<xsl:for-each select="tr[child::td]">
				<xsl:for-each select="td">
					<xsl:apply-templates/>
					<!--<xsl:message select="."/>
					<xsl:value-of select="$espace"/>-->
				</xsl:for-each>
				<!--<xsl:value-of select="$sautAGenerer"/>-->
			</xsl:for-each>
		</xsl:variable>

		<titre niveauBraille="3">
			<!-- écriture des entêtes non répétées (les premières)-->
			<!--<xsl:message select="'nbEntetes'"/>
			<xsl:message select="$nblEnt;"/>-->
			<xsl:if test="$nblEnt &gt;1">
				<xsl:for-each select="for $c in 1 to (($nblEnt -1) * $nbCol) return concat($entetes[$c],'&pt;')">
					<xsl:value-of select="."/>
				</xsl:for-each>
			</xsl:if>
		</titre>
		<ul style='tableau'>
			<!-- entetes et corps du tableau -->
			<!--<tableau mep="ligne">-->
			<xsl:variable as="xs:string" name="nomCol1" select="translate($entetes[($nblEnt -1) * $nbCol + 1],'&pt; ','')"/>
			<xsl:if test="not(string-length($nomCol1)=0)">
				<li>
					<xsl:value-of select="concat($espace,$espace,$nomCol1)"/>
				</li>
			</xsl:if>
			<!-- entetes à répéter -->
			<xsl:variable name="entetesRep" as="xs:string*">
				<xsl:for-each select="$entetes[position() &gt; ($nblEnt -1) * $nbCol]">
					<xsl:value-of select="."/>
				</xsl:for-each>
			</xsl:variable>
			<!-- nom des entetes -->
			<li>
				<xsl:value-of select="string-join($entetesRep,'&pt23;')"/>
			</li>
			<li>
			<xsl:for-each select="$corps">
				<xsl:variable name="pos" as="xs:integer" select="position()"/>
				<xsl:variable name="nomCol" select="$entetesRep[($pos -1) mod $nbCol + 1]"/>
					<xsl:value-of select="if($nomCol='')  then concat(.,'&pt23;&pt;') else concat($nomCol,'&pt25;&pt;',.,'&pt23;&pt;')"/>
					<!-- dernière colonne -->
					<xsl:if test="position() mod $nbCol = 0 and not(position() = last())"><xsl:text disable-output-escaping="yes">&lt;/li&gt;&#10;			&lt;li&gt;</xsl:text></xsl:if>
			</xsl:for-each>
			</li>
		</ul>
	</xsl:template>


	<xsl:template match="tableau" mode="colonne">
		
		<tableau mep="col">
			<xsl:value-of select="$debTable"/>
			
			<xsl:variable name="nbCol" select="count(tr[1]/th)" as="xs:integer"/><!-- nombre de colonnes du tableau -->
			<!-- transcription des données du tableau -->
			<!-- entètes -->
			<xsl:variable name="entetes" as="xs:string*">
				<xsl:for-each select="tr[child::th]">
					<xsl:for-each select="th">
						<xsl:variable name="resu">
							<xsl:apply-templates/>
						</xsl:variable>
						<xsl:value-of select="concat($resu,if (position() mod ($nbCol) =0) then $sautAGenerer else $espace)"/>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:variable>
			<!--<xsl:message select="$entetes"/>-->
			<!-- corps du tableau -->
			<xsl:variable name="nblEnt" select="count(tokenize(string-join($entetes,''),$sautAGenerer)) -1" as="xs:integer"/><!-- nombre de lignes d'entête -->
			<!--<xsl:message select="$nblEnt"/>
			<xsl:message select="$nbCol"/>-->
			<xsl:variable name="corps" as="xs:string*">
				<xsl:for-each select="tr[child::td]">
					<xsl:for-each select="td">
						<xsl:apply-templates/>
						<!--<xsl:message select="."/>
						<xsl:value-of select="$espace"/>-->
					</xsl:for-each>
					<!--<xsl:value-of select="$sautAGenerer"/>-->
				</xsl:for-each>
			</xsl:variable>

			<xsl:variable name="lgColEnt" as="xs:integer*" select="nat:getColSize($entetes,$nbCol)"/>
			<xsl:variable name="lgColCorps" as="xs:integer*" select="nat:getColSize($corps, $nbCol)"/>
			<!--<xsl:message select="$lgColEnt"/>
			<xsl:message select="$lgColCorps"/>-->

			<xsl:for-each select="$entetes">
				<xsl:value-of select="."/>
			</xsl:for-each>
			<xsl:for-each select="$corps">
				<xsl:value-of select="."/>
				<xsl:value-of select="if (position() mod ($nbCol) =0) then $sautAGenerer else $espace"/>
			</xsl:for-each>
			<xsl:value-of select="$finTable"/>
		</tableau>
	</xsl:template>

	<xsl:template match="td | tr | th">
		<xsl:apply-templates/>
	</xsl:template>
<xsl:template match="th">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="vide"/><!--<xsl:text>A</xsl:text></xsl:template>-->

<xsl:template name="espace">
	<xsl:param name="position" select="1" as="xs:integer" />
	<!--<xsl:message select="('ESP ''',.,'''',@doSpace, 'suiv', following::*[$position])"/>-->
	<xsl:choose>
		<xsl:when test="@doSpace='false'"/>
		<!--<xsl:when test="not(following::*[1]=text() or following::*[1]=node())">
		</xsl:when>-->
		<!--<xsl:when test="string(following::*[$position]) ='' or local-name(following::*[$position])='lit'">-->
		<xsl:when test="local-name(following::*[$position])='lit' or following::*[$position][not(string(.))]">
			<xsl:call-template name="espace">
				<xsl:with-param name="position" select="$position + 1"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="following::*[$position]='-' and local-name(following::*[$position+1])='ponctuation'"/>
		<xsl:when test="local-name(following::*[$position])='mot' or local-name(following::*[$position])='math' or following::*[$position]='-'">
			<xsl:if test="not(following::*[$position]/@attr='exp' or following::*[$position]/@attr='ind' or 
				(.='&quot;' and (count(preceding-sibling::ponctuation[.='&quot;']) mod 2)=0))"><!-- TODO ET qu'on est pas avec une locution et en abrégé -->
				<xsl:text>&pt;</xsl:text>
			</xsl:if>
		</xsl:when>
		<!-- espaces avec les pponctuations -->
		<xsl:when test="local-name(following::*[$position])='ponctuation'">
			<xsl:if test="contains('¡¿([{«“‘&lsquo;',following::*[$position])" >
				<xsl:text>&pt;</xsl:text>
			</xsl:if>
			<!--  cas particulier des guillements non orientés -->
			<xsl:if test="following::*[$position]='&quot;'">
				<xsl:variable name="ouvrant" as="xs:integer">
					<xsl:value-of select="count(preceding-sibling::ponctuation[.='&quot;']) mod 2"/>
				</xsl:variable>
				<!-- Espace si le nombre de guillements précédents est pair (ouverture) -->
				<xsl:if test="$ouvrant = 0">
					<xsl:text>&pt;</xsl:text>
				</xsl:if>
			</xsl:if>
		</xsl:when>
	</xsl:choose>
</xsl:template>
	
</xsl:stylesheet>
