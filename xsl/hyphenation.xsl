<?xml version='1.0' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:nat='http://natbraille.free.fr/xsl' 
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:xs='http://www.w3.org/2001/XMLSchema'>
<!-- version: 1.0.1 -->
<xsl:output method="text" encoding="UTF-8" indent="no"/>
<xsl:param name="debug" as="xs:boolean" select="false()"/>

<xsl:include href="nat-functs.xsl"/>

<!-- patterns utilisés pour hyphenate ; un exemple en xquery et un en xslt -->
<xsl:variable name="patterns0" select="for $pat in $patterns return nat:insert-char(translate($pat,'1234',''),'0')" as="xs:string*"/>
<xsl:variable name="patterns0nd" as="xs:string*">
	<xsl:for-each select="$patterns">
		<xsl:value-of select="nat:insert-char-if-not-digit(.,'0')" />
	</xsl:for-each>
</xsl:variable>

<!--<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>-->

<xsl:template match="word">
	<xsl:value-of select="nat:hyphenate(.,'-')"/>
</xsl:template>

<!-- *********** function de coupure litteraire basée sur liste de pattern de coupure ****************** -->
<xsl:function name="nat:hyphenate" as="xs:string" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="mot" as="xs:string" /><!-- le mot à couper -->
	<xsl:param name="cp" as="xs:string" /><!-- le caractère de coupe -->

	<xsl:if test="$debug"><xsl:message select="concat('{mot: ',$mot,'} -------')"/></xsl:if>
	<xsl:choose>
		<!-- le mot contient des caractères qui vont faire planter la fonction -->
		<xsl:when test="contains(translate($mot,'234','111'),'1')">
			<xsl:value-of select="$mot"/>
		</xsl:when>
		<!-- pas de caractère interdit -->
		<xsl:otherwise>
			<!-- préparation du mot -->
			<xsl:variable name="motCoup" select="nat:insert-char($mot,'0')" as="xs:string"/>
			<!--<xsl:message select="$motCoup"/>-->
			<xsl:variable name="coupures" as="xs:string*">
				<xsl:for-each select="$patterns">
					<xsl:variable name="i" select="position()" as="xs:integer"/>
					<!--<xsl:message select="concat('position:',position(),' pattern:',$patterns[$i])"/>-->
					<!-- <xsl:variable name="pattern" select="nat:insert-char(translate($patterns[$i],'1234',''),'0')" as="xs:string"/> -->
					<!--<xsl:message select="$pattern"/>-->
					<xsl:variable name="resu" as="xs:string">
						<xsl:value-of select="fn:replace($motCoup,$patterns0[$i],$patterns0nd[$i])"/>
					</xsl:variable>
					<xsl:if test="not($resu = $motCoup)">
						<xsl:value-of select="$resu"/>
						<xsl:if test="$debug">
							<xsl:message select="concat(' * ',position(),
								': {pattern: ',$patterns[$i],
								'} {resultat:', $resu,'}')"/>
								<!-- '} {pattern reformulé: ',string-join($pattern,''), -->
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable>

			<!--<xsl:message select="string-join($coupures,';')"/>-->
			<!-- insertion des coupures -->
			<xsl:variable name="coupNet" as="xs:string*" select="
				for $coup in $coupures 
				return fn:replace(
					fn:replace(
						fn:replace(
							fn:replace(
								fn:replace($coup,'^0|^1|^2|^3|^4',''),'01','1'),'02','2'),'03','3'),'04','4')"/>
			<!--<xsl:message select="$coupNet"/>-->
			
			<xsl:variable name="coupureFin">
				<xsl:for-each select="string-to-codepoints($coupNet[1])">
					<xsl:variable name="i" select="position()" as="xs:integer"/>
					<xsl:choose>
						<xsl:when test="position() mod 2 = 1"><xsl:value-of select="codepoints-to-string(.)"/></xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="max(for $c in $coupNet return codepoints-to-string(string-to-codepoints($c)[$i]))"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="coupureOK" select="translate(fn:replace($coupureFin,'1$|3$',''),'13024',concat($cp,$cp))"/>
			<!-- pour le débuggage des règles -->
			<xsl:if test="$debug"><xsl:message select="concat('après LIANG: ',$coupureFin,' : ',$coupureOK)"/></xsl:if>
			<xsl:value-of select="if ($coupureOK = '') then $mot else $coupureOK"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

</xsl:stylesheet>