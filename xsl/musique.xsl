<?xml version="1.1" encoding="utf-8"?>

<!--
Celle là fait l'applatissement du musicXML au niveau de la mesure.
tous les tags <measure num="oo">  </measure> sont transformés en <measurebar num="oo"/>
-->


<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
		xmlns:xs='http://www.w3.org/2001/XMLSchema'
		xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
		xmlns:fn='http://www.w3.org/2005/xpath-functions'>
  <xsl:output method="xml" indent="yes" />
  <xsl:include href="musicxml2.xsl" /> 

  <xsl:strip-space elements="*" />

  
  <!-- Score-partwise -->
  <xsl:template match="score-partwise">  
    <xsl:variable name="flattened" >
      <xsl:element name="flat-score-partwise">
	<xsl:copy-of select="child::work" />
	<xsl:apply-templates mode="flatten"/>
      </xsl:element>
    </xsl:variable>
    <!--
	<xsl:message select="$flattened" /> 
    -->
    <xsl:apply-templates select="$flattened" />
  </xsl:template>

  <!-- Part -->
  <xsl:template match="part" mode="flatten" >    

    <!-- part name -->
    <xsl:element name = "part" >
      <xsl:element name = "name" >
	<xsl:call-template name="getScorePartNameById">    
	  <xsl:with-param name="id" select="@id" />
	</xsl:call-template>
      </xsl:element>
      
      <!-- Measures -->
      <xsl:for-each select="measure">
	<xsl:element name = "measure_bar" >
	  <xsl:attribute name="number" select="@number"/>
	</xsl:element>
	<xsl:copy-of select="./*" />
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  
  
  <xsl:template match="work|identification" mode="flatten">
  </xsl:template>

  <!--
      gets part name by id
  -->
  <xsl:template name="getScorePartNameById">
    <xsl:param name="id" as="xs:string"/>
    <xsl:value-of select="//score-partwise/part-list/score-part[@id=$id]/part-name"/>
  </xsl:template>

</xsl:stylesheet>