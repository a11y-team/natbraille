<?xml version='1.1' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Raphaël Mina
 * Contact: raphael.mina@gmail.com
 *          natbraille.Free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->
<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd">
<!--
<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd"
[
  <!ENTITY % table_braille PUBLIC "table braille" "./tablesBraille/Brltab.ent">
  %table_braille;
  
]>-->




<xsl:stylesheet version="2.0"
xmlns='http://www.w3.org/1999/xhtml'
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:lit='espacelit'
xmlns:functx='http://www.functx.com'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>



<xsl:import href="functions/functx-1.1alpha.xsl" /> <!-- functx functions -->
<xsl:import href="tanMaths.xsl"/><!-- transcription braille mathématique-->


<xsl:character-map name="remplacementMap">
  <xsl:output-character character="&lt;" string="&#60;"/>
  <xsl:output-character character="&gt;" string="&#62;"/>

</xsl:character-map>

<xsl:output
   method="xhtml"
   doctype-public="-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN"
   indent="yes"
   encoding="UTF-8"
   media-type="text/html"
/>




<!--<xsl:output method="xhtml" encoding="UTF-8" indent="yes" use-character-maps="remplacementMap"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//FR"/>-->


<!-- On passe les symboles sémantiques braille par une variable pour éviter les problèmes de "unmatched quote"-->

<xsl:variable name="symboleMev">
  <xsl:text>&pt456;</xsl:text>
</xsl:variable>

<xsl:variable name="symboleFinPassage">
  <xsl:text>&pt6;&pt3;</xsl:text>
</xsl:variable>

<xsl:variable name="symboleMaj">
  <xsl:text>&pt46;</xsl:text>
</xsl:variable>

<xsl:variable name="symboleMajDouble">
  <xsl:text>&pt46;&pt46;</xsl:text>
</xsl:variable>

<xsl:variable name="symboleCleMaj">
  <xsl:text>&pt25;&pt46;</xsl:text>
</xsl:variable>

<xsl:variable name="symboleCleMev">
  <xsl:text>&pt25;&pt456;</xsl:text>
</xsl:variable>

<xsl:variable name="symboleNumerique">
  <xsl:text>&pt6;</xsl:text>
</xsl:variable>

<xsl:variable name="symboleFinNumerique">
  <xsl:text>&pt56;</xsl:text>
</xsl:variable>

<xsl:variable name="virgule">
  <xsl:text>&pt2;</xsl:text>
</xsl:variable>

<xsl:variable name="ptInterro">
  <xsl:text>&pt26;</xsl:text>
</xsl:variable>

<xsl:variable name="ptVirgule">
  <xsl:text>&pt23;</xsl:text>
</xsl:variable>

<xsl:variable name="point">
  <xsl:text>&pt256;</xsl:text>
</xsl:variable>

<xsl:variable name="deuxPoints">
  <xsl:text>&pt25;</xsl:text>
</xsl:variable>

<xsl:variable name="slash">
  <xsl:text>&pt34;</xsl:text>
</xsl:variable>

<xsl:variable name="ptExcla">
  <xsl:text>&pt235;</xsl:text>
</xsl:variable>

<xsl:variable name="prefixeFormule">
  <xsl:text>&pt6;&pt3;</xsl:text>
</xsl:variable>


<xsl:template match="/">
	<html xml:lang="FR" >
	  <body>
	  <xsl:apply-templates/>
	  </body>
	</html>
</xsl:template>


<xsl:template match="doc:doc">

	  <xsl:apply-templates/>

</xsl:template>


<xsl:template match="phrase">

  <p>
    <xsl:apply-templates/>
  </p>


</xsl:template>




<xsl:template match="lit">
	  <xsl:apply-templates select="*[1]">
	      <xsl:with-param name="fin" select="true()" tunnel="yes"/>
	      <xsl:with-param name="passageMaj" select="false()" tunnel="yes"/>
	      <xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
	  </xsl:apply-templates>
</xsl:template>

<xsl:template match="mot">
  <xsl:param name="fin" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>

	<xsl:if test="preceding-sibling::* and string(.)"><!-- TODO application règles des espaces de puncts, BRUNO -->
	    <xsl:text> </xsl:text>
	</xsl:if>

	
	<xsl:call-template name="debrailler">
	    <xsl:with-param name="contenu" select="functx:substring-after-match(.,'^[&pt;]+')"/>
	    <xsl:with-param name="maj" select="false()" tunnel="yes"/>
	    <xsl:with-param name="num" select="false()" tunnel="yes"/>
	</xsl:call-template>

</xsl:template>

<!--
<xsl:template match="ponctuation">
  <xsl:param name="fin" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>
  
    <xsl:if test="not(contains(translate(.,$point,$virgule),$virgule))">
      <xsl:text> </xsl:text>
    </xsl:if>
    
    <xsl:call-template name="bijection">
	    <xsl:with-param name="contenu" select="."/>
	    <xsl:with-param name="maj" select="false()" tunnel="yes"/>
	    <xsl:with-param name="num" select="false()" tunnel="yes"/>
    </xsl:call-template>
    
</xsl:template>-->

<xsl:template name="debrailler">

  <xsl:param name="contenu" as="xs:string"/>
  <xsl:param name="maj" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="num" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="fin" as="xs:boolean" tunnel="yes"/>

  <xsl:choose>
  
    <!-- Formule mathématique ?-->
    <xsl:when test="substring($contenu,1,2)=$prefixeFormule">

    <m:math>
      <xsl:call-template name="parser">
	<xsl:with-param name="contenu" select="substring($contenu,3)"/>
      </xsl:call-template>
    </m:math>

      <xsl:apply-templates select="following-sibling::*[1]">
	  <xsl:with-param name="fin" select="true()" tunnel="yes"/>
      </xsl:apply-templates>

    </xsl:when>


    <!--Contient une clé de mise en évidence ?-->
    <xsl:when test="contains($contenu,$symboleCleMev)">
    
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-before($contenu,$symboleCleMev)"/>
	   <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	</xsl:call-template>
	
	<xsl:text>&lt;em&gt;</xsl:text>
	
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-after($contenu,$symboleCleMev)"/>
	   <xsl:with-param name="passageMev" select="true()" tunnel="yes"/>
	</xsl:call-template>
      
    </xsl:when>
  
  
    <!--Contient une clé majuscule-->
    <xsl:when test="contains($contenu,$symboleCleMaj)">
      
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-before($contenu,$symboleCleMaj)"/>
	   <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	</xsl:call-template>
	
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-after($contenu,$symboleCleMaj)"/>
	   <xsl:with-param name="passageMaj" select="true()" tunnel="yes"/>
	</xsl:call-template>
	
    </xsl:when>
  
  
    <!--Contient une partie en exposant ?-->
    <!-- TODO important de peut-être remonter l'exposant pour traiter le cas ou les
    exposant contiennent des majuscules-->
    <xsl:when test="contains($contenu,'&pt4;')">
    
    <xsl:choose>
	    <!-- Les caractères qui interrompent la mise en indice sont le trait d'union,
	    la barre oblique et les signes numériques. Il convient donc de traiter le cas
	    où on est en passage numérique et l'autre cas.-->
	    <xsl:when test="$num">
		    <!--Premiere partie du mot, avant la mise en exposant-->
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="substring-before($contenu,'&pt4;')"/>
		      <xsl:with-param name="fin" select="false()" tunnel="yes"/>
		</xsl:call-template>
		
		<sup>
		    <!--Seconde partie du mot, avant l'éventuelle fin de mise en exposant-->
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="functx:substring-before-if-contains(translate(substring-after($contenu,'&pt4;'),'&pt235;&pt36;&pt35;&pt25;&pt2356;','&pt34;&pt34;&pt34;&pt34;'),'&pt34;')"/>
		      <xsl:with-param name="fin" select="false()" tunnel="yes"/>
		      
		 </xsl:call-template>
			 
		</sup>
		
		<!--Dernière éventuelle partie du mot, apres l'éventuelle fin de mise en exposant-->

		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="substring-after($contenu,functx:substring-before-if-contains(translate(substring-after($contenu,'&pt4;'),'&pt235;&pt36;&pt35;&pt25;&pt2356;','&pt34;&pt34;&pt34;&pt34;&pt34;'),'&pt34;'))"/>
		 </xsl:call-template>
	    </xsl:when>
	    
	    <xsl:otherwise>
	    	
		<!--Premiere partie du mot, avant la mise en exposant-->
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="substring-before($contenu,'&pt4;')"/>
		      <xsl:with-param name="fin" select="false()" tunnel="yes"/>

		</xsl:call-template>
		
		<sup>
		    <!--Seconde partie du mot, avant l'éventuelle fin de mise en exposant-->
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="functx:substring-before-if-contains(translate(substring-after($contenu,'&pt4;'),'&pt36;','&pt34;'),'&pt34;')"/>
		      <xsl:with-param name="fin" select="false()" tunnel="yes"/>

		 </xsl:call-template>
			 
		</sup>
		<!--Dernière éventuelle partie du mot, après l'éventuelle fin de mise en exposant-->
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="substring-after($contenu,functx:substring-before-if-contains(translate(substring-after($contenu,'&pt4;'),'&pt36;','&pt34;'),'&pt34;'))"/>
		 </xsl:call-template>

	    </xsl:otherwise>  
	</xsl:choose>
    </xsl:when>
    
    

  
    <!--Contient un double préfixe majuscule-->
    <xsl:when test="contains($contenu,$symboleMajDouble)">
    
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-before($contenu,$symboleMajDouble)"/>
	   <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	</xsl:call-template>
	
	
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="functx:substring-before-if-contains(substring-after($contenu,$symboleMajDouble),$symboleFinPassage)"/>
	   <xsl:with-param name="maj" select="true()" tunnel="yes"/>
	   <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	</xsl:call-template>
	
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-after($contenu,$symboleFinPassage)"/>
	   <xsl:with-param name="maj" select="false()" tunnel="yes"/>
	</xsl:call-template>

    </xsl:when>
  

  
    <!--Contient un seul préfixe majuscule-->
    <xsl:when test="contains($contenu,$symboleMaj)">
	
	 
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-before($contenu,$symboleMaj)"/>
	   <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	</xsl:call-template>
	
	
	 
	<xsl:choose>
	
  	    <!-- Un symbole de majuscule dans un passage en majuscule indique la fin de ce passage-->
	    <xsl:when test="$passageMaj">
	      
	      <xsl:call-template name="debrailler">
		 <xsl:with-param name="contenu" select="substring-after($contenu,$symboleMaj)"/>
		 <xsl:with-param name="maj" select="true()" tunnel="yes"/>
		 <xsl:with-param name="passageMaj" select="false()" tunnel="yes"/>
	      </xsl:call-template>  
	      
	    </xsl:when>
	  
	    <xsl:otherwise>
	    
	      <xsl:call-template name="debrailler">
		 <xsl:with-param name="contenu" select="substring($contenu,functx:index-of-string-first($contenu,$symboleMaj)+1,1)"/>
		 <xsl:with-param name="maj" select="true()" tunnel="yes"/>
		 <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	      </xsl:call-template> 
	      
	      <xsl:call-template name="debrailler">
		 <xsl:with-param name="contenu" select="substring($contenu,functx:index-of-string-first($contenu,$symboleMaj)+2)"/>
		 <xsl:with-param name="maj" select="false()" tunnel="yes"/>
	      </xsl:call-template> 
	      
	    </xsl:otherwise>
	    
	 </xsl:choose>
    </xsl:when>
  
  
    <!-- Contient un signe de mise en évidence ? -->
    <xsl:when test="contains($contenu,$symboleMev)">
    
      <xsl:choose>
	<!-- Si on est dans un passage en évidence, le pt456 signifie une fin de passage -->
	  <xsl:when test="$passageMev">
	  
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="substring-before($contenu,$symboleMev)"/>
		      <xsl:with-param name="fin" select="false()" tunnel="yes"/>
		</xsl:call-template>
		
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="substring-after($contenu,$symboleMev)"/>
		      <xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
		      <xsl:with-param name="fin" select="false()" tunnel="yes"/>
		</xsl:call-template>
		
		<!-- On est obligé de passer par la character map pour afficher la balise, car
		sinon le parseur signale que la balise <em> ouvrante n'existe pas dans ce bloc.-->
		<xsl:text>&lt;/em&gt;</xsl:text>
		
		<!-- Appel du template uniquement pour passer au mot suivant. Malin non ?-->
		<xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu">
			  <xsl:value-of select="''"/>
		      </xsl:with-param>
		      <xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
		</xsl:call-template>
		 
	  </xsl:when> 
	  
	  <xsl:otherwise>
	  
		<!-- Cas où le caractere pt456 marque le début d'un passage en évidence.-->
		    <xsl:call-template name="debrailler">
			<xsl:with-param name="contenu" select="substring-before($contenu,$symboleMev)"/>
			<xsl:with-param name="fin" select="false()" tunnel="yes"/>
		    </xsl:call-template>

		    <em>
	
		    <xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="functx:substring-before-if-contains(substring-after($contenu,$symboleMev),$symboleFinPassage)"/>
		      <xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
		      <xsl:with-param name="fin" select="false()" tunnel="yes"/>
		    </xsl:call-template>
		    
		    </em>
		    
		    <xsl:call-template name="debrailler">
		      <xsl:with-param name="contenu" select="substring-after($contenu,$symboleFinPassage)"/>
		      <xsl:with-param name="passageMev" select="false()" tunnel="yes"/>
		    </xsl:call-template>
		    
	  </xsl:otherwise>
	   
      </xsl:choose>
    
    </xsl:when>
    
    
    <!--Contient le symbole numérique-->
    <xsl:when test="contains($contenu,$symboleNumerique)">
    
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-before($contenu,$symboleNumerique)"/>
	   <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	</xsl:call-template>
	
	<xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="functx:substring-before-if-contains(substring-after($contenu,$symboleNumerique),$symboleFinNumerique)"/>
	   <xsl:with-param name="num" select="true()" tunnel="yes"/>
	   <xsl:with-param name="fin" select="false()" tunnel="yes"/>
	</xsl:call-template>
	
	
      <xsl:call-template name="debrailler">
	   <xsl:with-param name="contenu" select="substring-after($contenu,$symboleFinNumerique)"/>
	   <xsl:with-param name="num" select="false()" tunnel="yes"/>
	</xsl:call-template>

    </xsl:when>
    
    <xsl:otherwise>
    <!--Si on arrive jusqu'ici, c'est que le mot ne contient pas ou plus de chose remarquables.
        On opère alors une bijection de remplacement grâce aux paramètres glanés précédemment-->

      <xsl:call-template name="bijection">
	    <xsl:with-param name="contenu" select="$contenu"/>
      </xsl:call-template>
    </xsl:otherwise>
  
  </xsl:choose>
 
</xsl:template>




<xsl:template name="bijection">

  <xsl:param name="contenu" as="xs:string"/>
  <xsl:param name="maj" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="num" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMev" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="passageMaj" as="xs:boolean" tunnel="yes"/>
  <xsl:param name="fin" as="xs:boolean" tunnel="yes"/>

    
  <!--On définit l'alphabet de remplacement par partie, en fonction des paramètres majuscule et numérique-->
  <xsl:variable name="seqAlphabet" as="xs:string*">
      <xsl:choose>
	  <!-- En mode majuscule, l'alphabet de remplacement est en majuscule-->
	  <xsl:when test="$maj or $passageMaj">
	      <xsl:value-of select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÇÉÀÈÙ'"/>
	  </xsl:when>
	  <xsl:otherwise>
	      <xsl:value-of select="'abcdefghijklmnopqrstuvwxyzçéàèù'"/>
	  </xsl:otherwise>
      </xsl:choose>
      <xsl:choose>
	  <!-- En mode numérique, on remplace par des nombres, sinon par les lettres de la quatrième série
	  et d'un espace, le 0 n'ayant pas d'équivalent-->
	  <xsl:when test="$num">
	      <xsl:text> 1234567890</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	      <xsl:choose>
		<xsl:when test="$maj">
		    <xsl:text>'ÂÊÎÔÛËÏÜŒ</xsl:text>
		</xsl:when>
		<xsl:otherwise>
		    <xsl:text>'âêîôûëïüœ</xsl:text>
		</xsl:otherwise>
	      </xsl:choose>
	      
	      <!--Sortie d'une chaîne vide pour compenser le caractère 0-->
	      <xsl:text> </xsl:text>
	  </xsl:otherwise>
	</xsl:choose>
	
	<!-- partie commune : parenthèses, etc...-->
	<xsl:text>,?;.:/!"()@-</xsl:text>
	
  </xsl:variable>
  
  <!--On concatène les différentes partie de l'alphabet de remplacement-->
  <xsl:variable name="alphabetRemplacement" select="string-join($seqAlphabet,'')"/>
  
  <xsl:variable name="alphabetBraille">
<xsl:text>&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12346;&pt123456;&pt12356;&pt2346;&pt23456;&pt3;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt3456;&pt2;&pt26;&pt23;&pt256;&pt25;&pt34;&pt235;&pt2356;&pt236;&pt356;&pt345;&pt36;</xsl:text></xsl:variable>
  
  <xsl:value-of select="translate($contenu,$alphabetBraille,$alphabetRemplacement)"/>
  

  <xsl:if test="$fin">
 
      <xsl:apply-templates select="following-sibling::*[1]">
	  <xsl:with-param name="fin" select="true()" tunnel="yes"/>
      </xsl:apply-templates>
      
  </xsl:if>

    
</xsl:template>

</xsl:stylesheet>
