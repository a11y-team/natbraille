<?xml version='1.0' encoding="UTF-8" ?>
<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd">
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version 1.5 -->


<xsl:stylesheet version="2.0" 

xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:nat='http://natbraille.free.fr/xsl' 
xmlns:saxon='http://icl.com/saxon'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:xhtml="http://www.w3.org/1999/xhtml" 
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:doc='espaceDoc'> 

<xsl:import href="functions/functx-1.1alpha.xsl" /> <!-- functx functions -->
<xsl:param name="dtd" select="'xsl/mmlents/windob.dtd'" as="xs:string"/>
<xsl:param name="processImage" select="false()" as="xs:boolean"/>
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="no"/>


<xsl:strip-space elements = "xhtml:p xhtml:head xhtml:meta xhtml:title xhtml:link xhtml:style xhtml:ul xhtml:ol xhtml:li m:* m:math m:semantics m:mrow xhtml:table xhtml:td xhtml:tbody xhtml:tr xhtml:thead xhtml:th" />

<xsl:variable name="l_ponct">¡¿”&quot;’,.:;!?»…)]}\}«“‘([{}&lsquo;&rsquo;&acute;&prime;&mldr;&vellip;&hellip;&dtdot;&ldots;&ctdot;&utdot;</xsl:variable>

<xsl:variable name="styles" as="xs:string*">
	<!-- cette variable va contenir tous les styles, un par ligne, en commençant par le nom du style -->
	<xsl:for-each select="functx:lines(string(/xhtml:html/xhtml:head/xhtml:style/text()))">
		<xsl:if test="string-length(normalize-space(.)) >0">
			<xsl:value-of select="normalize-space(.)" />
		</xsl:if>
	</xsl:for-each>
</xsl:variable>

<xsl:template match="/">
	<xsl:text disable-output-escaping="yes">
		&lt;!DOCTYPE doc:doc SYSTEM "
	</xsl:text>
	<xsl:value-of select="$dtd"/>
	<xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
	<doc:doc xmlns:doc="espaceDoc">
	<xsl:apply-templates select="*|text()|processing-instruction()" />
	</doc:doc>
</xsl:template>
<!--
<xsl:template match="xhtml:table|xhtml:tr|xhtml:div|xhtml:html|xhtml:body">
	<xsl:apply-templates/>
</xsl:template>-->
<xsl:template match="*[@class='echap']" mode="#all">
	<echap>
		<xsl:value-of select='.'/>
	</echap>
</xsl:template>

<xsl:template match="xhtml:head/xhtml:title">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="xhtml:head|xhtml:tr">
</xsl:template>

<xsl:template match="xhtml:div|xhtml:html|xhtml:body">
	<xsl:apply-templates/>
</xsl:template>
<!-- Je commente pour l'instant -->
<xsl:template match="xhtml:*[contains(@style,'break-before:page') and not(self::p)] | xhtml:p[contains(@style,'break-before:page') and not(local-name(..)='li')]" priority="2" mode="#all">
	<page-break/>
	<xsl:next-match/>
</xsl:template>

<xsl:template match="xhtml:*[contains(@style,'break-after:page') and not(self::p)] | xhtml:p[contains(@style,'break-after:page') and not(local-name(..)='li')]" priority="1.5" mode="#all">
	<xsl:next-match/>
	<page-break/>
</xsl:template>

<xsl:template match="xhtml:thead|xhtml:tbody|xhtml:table">
	<!-- TODO améliorer la différenciation des types de tableaux -->
	<xsl:choose>
		<xsl:when test="not(child::xhtml:tbody or child::xhtml:thead) and (count(descendant::*[local-name()='math'])&gt;0)">
			<!--<tableau type="math">-->
<phrase><math xmlns="http://www.w3.org/1998/Math/MathML">
			<mtable>
				<xsl:for-each select="xhtml:tr|xhtml:th">
					<mtr>
						<xsl:for-each select="xhtml:td">
							<mtd>
								<xsl:for-each select="xhtml:p">
									<xsl:choose>
										<xsl:when test="child::m:math">
											<xsl:copy-of select="child::m:math/*"/>
										</xsl:when>
										<!--<xsl:when test="string-length(normalize-space(.)) &gt; 0"> TODO trouver pourquoi il fait des mi vides
											<mi><xsl:value-of select="."/></mi>
										</xsl:when>-->
									</xsl:choose>
								</xsl:for-each>
							</mtd>
						</xsl:for-each>
					</mtr>
				</xsl:for-each>
			</mtable>
			</math>
			</phrase>
			<!--</tableau>-->
		</xsl:when>
		<xsl:otherwise>
			<!-- c'est un tableau littéraire -->
			<tableau type="lit">
				<!-- dimensions réelles du tableau -->
				<xsl:variable name="nbC" as="xs:integer" select="xs:integer(sum(for $r in xhtml:tr[position() = 1]/xhtml:td return if($r/@rowspan) then $r/@rowspan else 1))"/>
				<xsl:variable name="nbL" as="xs:integer" select="xs:integer(sum(for $r in xhtml:tr/xhtml:td return  max(($r/@colspan,1))*max((1,$r/@rowspan))) div $nbC)"/>
				<!--<xsl:message select="concat('colonnes', $nbC, ' lignes:',$nbL)"/>-->
				<!-- taille des colonnes en cellules suivie du nb de lignes fusionnées -->
				<xsl:variable as="xs:integer*" name="tCol" select="for $c in 1 to $nbC return 0"/>
				<!--<xsl:message select="concat('indice',string-join(for $c in $tCol return string($c),' '))"/>-->
				<!-- réécriture du tableau -->
				<xsl:variable name="tabComplet" >
					<xsl:call-template name="writeTabLn">
						<xsl:with-param name="nbC" select="$nbC"/>
						<xsl:with-param name="tCol" select="$tCol"/>
					</xsl:call-template>
				</xsl:variable>

				<!--<xsl:message select="'tableau:'"/>
				<xsl:message select="$tabComplet"/>-->
				<!-- récupération du nombre de ligne des entêtes: nb ligne de la 1ère cellule -->
				<xsl:variable name="nblEntetes" as="xs:integer" select="max((1,xs:integer(xhtml:tr[1]/xhtml:td/@rowspan)))"/><!-- nombre de ligne des entêtes -->

				<xsl:for-each select="$tabComplet/*">
					<xsl:variable name="pos" select="position()"/>
					<xsl:if test="$pos mod $nbC = 1">
						<xsl:text disable-output-escaping="yes">&lt;tr&gt;</xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$pos &lt;= $nbC * $nblEntetes">
							<th>
								<xsl:variable name="spanCol" select="if(following-sibling::phrase[1]) then 
									functx:index-of-node($tabComplet/*[position() &gt; $pos],following-sibling::phrase[1]) - $pos
									else 0"/>
								<xsl:if test="$spanCol &gt; 0">
									<xsl:attribute name="col" select="$spanCol + 1"/>
								</xsl:if>
								<xsl:copy-of select="."/>
							</th>
						</xsl:when>
						<xsl:otherwise>
							<td>
								<xsl:copy-of select="."/>
							</td>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$pos mod $nbC = 0">
						<xsl:text disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
					</xsl:if>
				</xsl:for-each>
			</tableau>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- écris les cellules en tenant compte des fusions, complète les trous avec des cellules vides -->
<xsl:template name="writeTabLn">
	<xsl:param name="nbC" as="xs:integer"/><!-- nombre de colonnes du tableau -->
	<xsl:param name="tCol" as="xs:integer*"/> <!-- liste contenant pour chaque case un entier; si >0, signifie qu'il faut écrire une case vide à cause des fusions -->
	<xsl:param name="col" as="xs:integer" select="1"/><!-- la colonne actuelle -->
	<xsl:param name="row" as="xs:integer" select="1"/><!-- la ligne actuelle -->
	<xsl:param name="pos" as="xs:integer" select="1"/><!-- position du prochain enfant de tr à traiter -->

	<!-- calcul de la prochaine cellule à traiter -->
	<xsl:variable name="nextCol" as="xs:integer" select="$col mod $nbC + 1"/>
	<xsl:variable name="nextRow" as="xs:integer" select="if ($nextCol = 1) then ($row + 1) else $row"/>
	<!--<xsl:message select="concat('*nextC:', $nextCol, ' nextR:', $nextRow)"/>-->
	<xsl:choose>
		<xsl:when test="($tCol[$col] &gt; 0)"><!-- c'est une cellule vide -->
			<vide/>
			<!-- reconstruction de tCol -->
			<xsl:variable name="nextTCol" as="xs:integer*" select="for $c in 1 to $nbC return 
				if ($nextRow &gt; $row) then max(($tCol[$c] -1,0))
				else $tCol[$c]"/>
			<!--<xsl:message select="concat('indice ',string-join(for $c in $nextTCol return string($c),' '))"/>-->
			<xsl:call-template name="writeTabLn">
				<xsl:with-param name="nbC" select="$nbC"/>
				<xsl:with-param name="tCol" select="$nextTCol"/>
				<xsl:with-param name="col" select="$nextCol"/>
				<xsl:with-param name="row" select="$nextRow" />
				<xsl:with-param name="pos" select="if ($nextCol = 1) then 1 else $pos" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise><!-- c'est une cellule à lire -->
			<xsl:if test="xhtml:tr[$row]/xhtml:td[$pos]"><!-- faux si il n'y a plus de cellules à traiter (fin du tableau)-->
				<xsl:variable name="cellule" select="xhtml:tr[$row]/xhtml:td[$pos]"/>
				<xsl:variable name="cellTr">
					<xsl:apply-templates select="$cellule"/>
				</xsl:variable>
				<xsl:apply-templates select="$cellule"/><!-- TODO : remplacer par cellTr -->
				<!-- reconstruction de tCol, PLUS COMPLIQUÉE car il faut prendre en compte les colspan et les rowspan de la cellule actuelle -->
				<xsl:variable name="nextTCol" as="xs:integer*" select="for $c in 1 to $nbC return 
					if ($nextRow &gt; $row) then 
						if (($c >= $col) and ($col +  $cellule/@colspan > $c) or $c = $col)
						then xs:integer(max(($cellule/@rowspan -1,0)))
						else max(($tCol[$c] -1,0))
					else
						if (($c >= $col) and ($col +  $cellule/@colspan > $c) or $c = $col)
						then xs:integer(max(($cellule/@rowspan,1)))
						else $tCol[$c]"/>
				<!--<xsl:message select="concat('indice',string-join(for $c in $nextTCol return string($c),' '))"/>-->
				<xsl:call-template name="writeTabLn">
					<xsl:with-param name="nbC" select="$nbC"/>
					<xsl:with-param name="tCol" select="$nextTCol"/>
					<xsl:with-param name="col" select="$nextCol"/>
					<xsl:with-param name="row" select="$nextRow" />
					<xsl:with-param name="pos" select="if ($nextCol = 1) then 1 else $pos + 1" />
				</xsl:call-template>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="retourneTableau">
	<xsl:param name="nbCol"/>
	<xsl:param name="colonne"/>
	
	<xsl:if test="not($colonne &gt; $nbCol)">
		<col>
		<xsl:for-each select="child::xhtml:tr">
			<xsl:for-each select="child::xhtml:td[position()=$colonne]">
				<ligne>
				<xsl:if test="(not(substring-before(substring-after(@style, 'border-right:'),';')='none' or substring-before(substring-after(@style, 'border-right:'),';')='') or not(substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border-left:'),';')='none' or substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border-left:'),';')='')) or not(substring-before(substring-after(@style, 'border:'),';')='none' or substring-before(substring-after(@style, 'border:'),';')='') or not(substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border:'),';')='none' or substring-before(substring-after(following-sibling::xhtml:td[1]/@style, 'border:'),';')='')">
					<xsl:attribute name="b-vert">1</xsl:attribute>
				</xsl:if>
				<xsl:if test="not(substring-before(substring-after(@style, 'border-bottom:'),';')='none')">
					<xsl:attribute name="b-hor">1</xsl:attribute>
				</xsl:if>
				<xsl:apply-templates/>
				</ligne>
			</xsl:for-each>
		</xsl:for-each>
		</col>
		<xsl:call-template name="retourneTableau">
			<xsl:with-param name="nbCol" select="$nbCol"/>
			<xsl:with-param name="colonne" select="$colonne + 1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>


<xsl:variable name="empty_string"/>

<xsl:template match="xhtml:h1|xhtml:h2|xhtml:h3|xhtml:h4|xhtml:h5|xhtml:h6|xhtml:h7|xhtml:h8|xhtml:h9">
	<titre>
		<!-- quel est le niveau du titre? -->
		<xsl:attribute name="niveauOrig">
			<xsl:choose>
				<xsl:when test="substring(local-name(.),2,1)='1'">1</xsl:when>
				<xsl:when test="substring(local-name(.),2,1)='2'">2</xsl:when>
				<xsl:when test="substring(local-name(.),2,1)='3'">3</xsl:when>
				<xsl:when test="substring(local-name(.),2,1)='4'">4</xsl:when>
				<xsl:otherwise>5</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<!-- le contenu n'est pas vide; pour simplifier, on crée forcement un tag lit suivi d'un tag mot, s'ils sont vides, on les supprimera dans la feuille de style de transcription -->
		<xsl:if test="@xml:lang='es-ES'">
			<xsl:attribute name="lang">
				<xsl:value-of select="string('es')"/>
			</xsl:attribute>
		</xsl:if>
		<lit>
			<xsl:for-each select="*|text()">
				<!--<xsl:sort select="position()"/>-->
				<!--<xsl:value-of select="."/>-->
				<xsl:choose>
					<xsl:when test="node()">
						<xsl:apply-templates select="." mode="phrase"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="remplaceEspace">
							<xsl:with-param name="chaine">
								<xsl:value-of disable-output-escaping="yes" select="." />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</lit>
	</titre>
</xsl:template>

<xsl:template match="xhtml:li" mode="#all">
	<xsl:if test="contains(xhtml:p/@style,'break-before:page')"><page-break /></xsl:if>
	<li>
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="self::xhtml:ul or self::xhtml:ol">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:when test="node()">
					<lit>
						<xsl:apply-templates select="." mode="phrase"/>
					</lit>
				</xsl:when>
				<xsl:otherwise>
					<lit>
						<xsl:call-template name="remplaceEspace">
							<xsl:with-param name="chaine">
								<xsl:value-of disable-output-escaping="yes" select="." />
							</xsl:with-param>
						</xsl:call-template>
					</lit>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</li>
	<xsl:if test="contains(xhtml:p/@style,'break-after:page')"><page-break /></xsl:if>
</xsl:template>

<xsl:template match="xhtml:ul|xhtml:ol" mode="#all">
	<xsl:element name="{local-name(.)}">
		<xsl:apply-templates />
	</xsl:element>
</xsl:template>

<xsl:template match="xhtml:td|xhtml:pre|xhtml:p|xhtml:title">
	<!-- je vire le test du champ vide car c'est important dans certain cas (cellules de tableaux vides) en fait... -->
	<!--<xsl:if test="(not(string(.)=' ') and not(string(.)=''))">-->
	<!-- je vire pour garder les lignes vides <xsl:if test="not(string(.)='')">-->
	<xsl:choose>
		<xsl:when test="*[1][@class='MTConvertedEquation']">
			<xsl:choose>
				<xsl:when test="*[1] = '&lt;math&gt;'">
					<!--ouverture de phrase maths -->
					<xsl:text disable-output-escaping="yes">&lt;phrase&gt;</xsl:text>
					<!-- y a-t-il du contenu avant? -->
					<xsl:if test="text()[1]">
						<xsl:for-each select="text()[following-sibling::*[1][@class='MTConvertedEquation']]">
							<lit>
							<!--NE pas supprimer le commentaire; Bruno<xsl:choose>
									<xsl:when test="node()">
										<xsl:apply-templates select="." mode="phrase"/>
									</xsl:when>
									<xsl:otherwise>-->
										<xsl:call-template name="remplaceEspace">
											<xsl:with-param name="chaine">
												<xsl:value-of disable-output-escaping="yes" select="." />
											</xsl:with-param>
										</xsl:call-template>
									<!--</xsl:otherwise>
								</xsl:choose>-->
								</lit>
						</xsl:for-each>
					</xsl:if>
					<xsl:apply-templates select="*" mode="phrase"/>
				</xsl:when>
				<xsl:when test="*[1] = '&lt;/math&gt;'">
					<!-- y a-t-il du contenu après? -->
					<xsl:choose>
						<xsl:when test="text()">
							<xsl:apply-templates select="*[1]" mode="phrase"/>
							<xsl:for-each select="text()">
								<lit>
								<!-- NE pas supprimer le commentaire <xsl:choose>
										<xsl:when test="node()">
											<xsl:apply-templates select="." mode="phrase"/>
										</xsl:when>
										<xsl:otherwise>-->
											<xsl:call-template name="remplaceEspace">
												<xsl:with-param name="chaine">
													<xsl:value-of disable-output-escaping="yes" select="." />
												</xsl:with-param>
											</xsl:call-template>
										<!--</xsl:otherwise>
									</xsl:choose>-->
								</lit>
							</xsl:for-each>
							<xsl:apply-templates select="*[position()&gt;1]" mode="phrase"/>
						</xsl:when>
						<xsl:otherwise><xsl:apply-templates select="*" mode="phrase"/></xsl:otherwise>
					</xsl:choose>
					<!--fermeture de phrase maths -->
					<xsl:if test="not(*[2])">
						<xsl:text disable-output-escaping="yes">&lt;/phrase&gt;</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="*" mode="phrase"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="translate(string(.),'&nbsp; 	','')='' and not(xhtml:img)">
			<xsl:if test="empty((preceding-sibling::*[1]/xhtml:span[@class='MTConvertedEquation'], following-sibling::*[1]/xhtml:span[@class='MTConvertedEquation']))">
				<phrase />
			</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			<phrase>
				<!-- le contenu n'est pas vide; pour simplifier, on crée forcement un tag lit suivi d'un tag mot, s'ils sont vides, on les supprimera dans la feuille de style de transcription -->
				<xsl:if test="@xml:lang='es-ES'">
					<xsl:attribute name="lang">
						<xsl:value-of select="string('es')"/>
					</xsl:attribute>
				</xsl:if>
				<lit>
					<xsl:for-each select="xhtml:img|*|text()">
						<!--<xsl:sort select="position()"/>-->
						<!--<xsl:value-of select="."/>-->
						<xsl:choose>
							<xsl:when test="node()|self::xhtml:img">
								<xsl:apply-templates select="." mode="phrase"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="remplaceEspace">
									<xsl:with-param name="chaine">
										<xsl:value-of disable-output-escaping="yes" select="." />
									</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</lit>
			</phrase>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="xhtml:pre|xhtml:p|xhtml:h1|xhtml:h2|xhtml:h3|xhtml:h4|xhtml:h5|xhtml:h6|xhtml:h7|xhtml:h8|xhtml:h9|xhtml:title" mode="phrase">
	<!-- je vire le test du champ vide car c'est important dans certain cas (cellules de tableaux vides) en fait... -->
	<!--<xsl:if test="(not(string(.)=' ') and not(string(.)=''))">-->
	<xsl:if test="not(string(.)='')">
		<xsl:if test="local-name(.)='li'">
			<xsl:choose>
				<xsl:when test="local-name(parent::*[1])='ul'">
					<ponctuation type="puce">-</ponctuation>
				</xsl:when>
				<xsl:when test="local-name(parent::*[1])='ol'">
					<ponctuation>
						<xsl:attribute name="type">ordre</xsl:attribute>
						<xsl:attribute name="num">
							<xsl:value-of select="count(preceding-sibling::*[name()='li'])+1"/>
						</xsl:attribute>
						<xsl:text>-</xsl:text>
					</ponctuation>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:for-each select="*|text()|xhtml:img">
			<!--<xsl:sort select="position()"/>-->
			<!--<xsl:value-of select="."/>-->
			<xsl:choose>
				<xsl:when test="node()|self::xhtml:img">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine">
							<xsl:value-of disable-output-escaping="yes" select="." />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:span" mode="phrase">
	<xsl:choose>
		<!-- c'est des maths convertis par openoffice -->
		<xsl:when test="@class='MTConvertedEquation'">
			<xsl:variable name="expr">
				<xsl:choose>
					<xsl:when test=".='&lt;math&gt;'">
						<xsl:text disable-output-escaping="yes">&lt;math xmlns="http://www.w3.org/1998/Math/MathML" &gt;</xsl:text>
					</xsl:when>
					<xsl:otherwise><xsl:value-of select="text()"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:value-of select="$expr" disable-output-escaping="yes"/>
			
		</xsl:when>
		<xsl:when test="not(string(.)=' ') and not(string(.)='') or xhtml:img ">
		<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
			<xsl:for-each select="*|text()">
				<xsl:choose>
					<xsl:when test="node()">
						<xsl:apply-templates select="." mode="phrase"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="remplaceEspace">
							<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="xhtml:tt|xhtml:cite|xhtml:dfn|xhtml:samp|xhtml:code|xhtml:var" mode="phrase">
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="node()">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:a" mode="phrase">
	<!--<xsl:if test="text()">-->
	<xsl:call-template name="remplaceEspace">
		<xsl:with-param name="chaine">
				<xsl:value-of select="concat(.,'(',@href,')')" />
		</xsl:with-param>
	</xsl:call-template>
</xsl:template>

<xsl:template match="xhtml:img" mode="phrase">
	<xsl:if test="not(@alt=' ' or @alt='')">
		<xsl:call-template name="remplaceEspace">
			<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
					<xsl:value-of select="concat(.,'[ ',@alt,']')" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
	<xsl:if test="$processImage">
		<xsl:call-template name="remplaceEspace">
			<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
					<xsl:value-of select="concat('Conversion de l''image en Braille en annexe ',count(preceding::xhtml:img)+1)" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:img">
	<phrase>
		<lit>
			<xsl:if test="not(@alt=' ' or @alt='')">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
							<xsl:value-of select="concat(.,'[ ',@alt,']')" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$processImage">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine"><!-- espace après le [ pour que le [ soit bien considéré comme une ponctuation-->
							<xsl:value-of select="concat('Conversion de l''image en Braille en annexe ',count(preceding::xhtml:img)+1)" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</lit>
	</phrase>
</xsl:template>

<xsl:template match="xhtml:sup" mode="phrase">
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="node()">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrvalue" select="'exp'"/>
						<xsl:with-param name="attrname" select="'attr'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:sub" mode="phrase">
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="node()">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrvalue" select="'ind'"/>
						<xsl:with-param name="attrname" select="'attr'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:i" mode="phrase">
	<!-- on suppose qu'on est forcement dans une phrase -->
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="node()">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrvalue" select="'italique'"/>
						<xsl:with-param name="attrname" select="'attr'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:em" mode="phrase">
	<!-- on suppose qu'on est forcement dans une phrase -->
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="node()">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrvalue" select="'emph'"/>
						<xsl:with-param name="attrname" select="'attr'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:strong" mode="phrase">
	<!-- on suppose qu'on est forcement dans une phrase -->
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="node()">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name="attrvalue" select="'emph2'"/>
						<xsl:with-param name="attrname" select="'attr'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template match="xhtml:b" mode="phrase">
	<xsl:if test="not(string(.)=' ') and not(string(.)='')">
	<!-- Attention au caractère d'espace précédent, il faut le mettre dans le bon jeu de caractère sinon le test ne marche pas!!!! -->
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test="node()">
					<xsl:apply-templates select="." mode="phrase"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="remplaceEspace">
						<xsl:with-param name="chaine" select="translate(.,'&nbsp;',' ')" />
						<xsl:with-param name ="attrvalue" select="'gras'"/>
						<xsl:with-param name ="attrname" select="'attr'"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:if>
</xsl:template>
<!-- musique recopé tel quel -->
<xsl:template match="score-partwise">
		<xsl:copy-of select="."/>
</xsl:template>

<!--math recopiés tels quel-->
<xsl:template match="m:math" mode="phrase">
	<xsl:text disable-output-escaping="yes">&lt;/lit&gt;</xsl:text>
		<xsl:copy-of select="."/>
<!--
		<xsl:copy><!- disable-output-escaping="yes">->
			<xsl:apply-templates select="*|text()|processing-instruction()"/>
		</xsl:copy>-->
	<xsl:text disable-output-escaping="yes">&lt;lit&gt;</xsl:text>
</xsl:template>



<xsl:template name="remplaceEspace">
	<xsl:param name="chaine" />
	<xsl:param name="attrname" />
	<xsl:param name="attrvalue" />
	<xsl:variable name="chaine2">
		<!-- on remplace insécables, tabulation, et saut de ligne par des espaces, on vire les espaces du début -->
		<xsl:value-of select="fn:replace(translate($chaine,'&nbsp;	&#10;&#13;','    '),'^[ ]+','')"/>
	</xsl:variable>
	<xsl:variable name="chaines" as="xs:string*" select="tokenize($chaine2,'\s')"/>
	<xsl:for-each select="$chaines">
		<xsl:choose>
			<!-- ponctuation seule -->
			<xsl:when test=". =''"/>
			<xsl:when test="string-length(.)=1 and (contains(concat($l_ponct, '-'),substring(.,1,1)))">
				<ponctuation>
					<xsl:value-of select="." />
				</ponctuation>
			</xsl:when>
			<xsl:when test="string-length(.)=1 and starts-with(.,'''')">
				<ponctuation>&apos;</ponctuation>
			</xsl:when>
			<xsl:when test="string-length(.)=3 and substring(.,1,3)='...'">
				<ponctuation>...</ponctuation>
			</xsl:when>
			<!-- ponctuation au début du mot sans espace -->
			<xsl:when test="contains($l_ponct,substring(.,1,1))">
				<ponctuation>
				<xsl:value-of select="substring(., 1, 1)" />
				</ponctuation>
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,2,string-length(.))" />
					</xsl:with-param>
					<xsl:with-param name ="attrvalue" select="$attrvalue"/>
					<xsl:with-param name ="attrname" select="$attrname"/>
				</xsl:call-template>
			</xsl:when>
			<!-- ponctuation à la fin du mot sans espace -->
			<xsl:when test="string-length(.)&gt;2 and substring(., string-length(.)-2, string-length(.))='...'">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,1,string-length(.)-3)" />
					</xsl:with-param>
					<xsl:with-param name ="attrvalue" select="$attrvalue"/>
					<xsl:with-param name ="attrname" select="$attrname"/>
				</xsl:call-template>
				<ponctuation>
				<xsl:text>...</xsl:text>
				</ponctuation>
			</xsl:when>
			<xsl:when test="contains($l_ponct,substring(., string-length(.), string-length(.)))">
				<xsl:call-template name="remplaceEspace">
					<xsl:with-param name="chaine">
						<xsl:value-of select="substring(.,1,string-length(.)-1)" />
					</xsl:with-param>
					<xsl:with-param name ="attrvalue" select="$attrvalue"/>
					<xsl:with-param name ="attrname" select="$attrname"/>
				</xsl:call-template>
				<ponctuation>
				<xsl:value-of select="substring(., string-length(.), string-length(.))" />
				</ponctuation>
			</xsl:when>
			<xsl:otherwise>
				<mot>
					<xsl:if test="not(string($attrname)='')">
						<xsl:attribute name="{string($attrname)}">
							<xsl:value-of select="string($attrvalue)"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="."/>
				</mot>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>